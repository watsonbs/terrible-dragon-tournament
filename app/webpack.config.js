const webpack = require('webpack');
const path = require('path');

const BUILD_DIR = path.resolve(__dirname, '../public');
const APP_DIR = path.resolve(__dirname, 'src');

const config = {
  entry: `${APP_DIR}/index.jsx`,
  output: {
    path: BUILD_DIR,
    filename: 'bundle.js',
  },
  //   node: {
  //       fs: 'empty',
  //       tls: 'empty'
  //   },
  module: {
    rules: [
      {
        test: /\.(s*)css$/,
        include: APP_DIR,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.json$/,
        loader: 'json',
      },
      {
        test: /\.jsx?/,
        include: APP_DIR,
        loader: 'babel-loader',
        query: {
          presets: ['babel-preset-env', 'react'],
          plugins: ['syntax-dynamic-import', 'transform-object-rest-spread'],
        },
      },
      {
        test: /\.svg$/,
        loader: 'svg-inline-loader',
      },
    ],
  },
  resolve: {
    alias: {
      'td-core': `${APP_DIR}/td-core/td-core.js`,
      'app-core': `${APP_DIR}/app-core/app-core.js`,
    },
    extensions: ['.js', '.jsx', 'index.jsx', '.json'],
  },
};

module.exports = config;
