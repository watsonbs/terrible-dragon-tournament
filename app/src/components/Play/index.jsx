import React from 'react';
import { player } from 'app-core';
import GameHeader from './GameHeader';

const CommonsPot = () => import('./games/CommonsPot');
const Democrazy = () => import('./games/Democrazy');
const Pending = () => import('./games/Pending');

const gameComponents = {
  commonsPot: CommonsPot,
  democrazy: Democrazy,
  pending: Pending,
};

const loadGameComponent = typeId => {
  const getGameComponent = gameComponents[typeId];
  return getGameComponent().then(gameModule => gameModule.default);
};

class Play extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    player.getTournament(this.props.tournamentId).then(tournament => {
      const gameTypeId = tournament.GameState ? tournament.GameState.typeId : 'pending';
      const isAdmin = player.getUserId() === tournament.Administrator;
      loadGameComponent(gameTypeId).then(GameComponent =>
        this.setState({ tournament, GameComponent, isAdmin }),
      );
    });

    this.updateTournament = this.updateTournament.bind(this);
  }

  updateTournament(tournamentUpdates) {
    if (!tournamentUpdates) return;

    const tournament = {
      ...this.state.tournament,
      ...tournamentUpdates,
    };

    const existingGameType =
      this.state.tournament.GameState && this.state.tournament.GameState.typeId;
    const newGameType = tournamentUpdates.GameState && tournamentUpdates.GameState.typeId;

    if (existingGameType !== newGameType) {
      loadGameComponent(newGameType).then(GameComponent =>
        this.setState({ tournament, GameComponent }),
      );
    } else {
      this.setState({ tournament });
    }
  }

  render() {
    const tournament = this.state.tournament;
    const gameType = tournament && tournament.GameState && tournament.GameState.typeId;
    const Component = tournament && this.state.GameComponent ? this.state.GameComponent : null;
    return (
      <div>
        {Component ? (
          <div>
            <GameHeader
              gameName={tournament.TournamentName}
              tournamentId={this.props.tournamentId}
              gameType={gameType}
              nextTick={tournament.NextTick}
            />
            <Component tournament={tournament} isAdmin updateTournament={this.updateTournament} />
          </div>
        ) : (
          <div>loading...</div>
        )}
      </div>
    );
  }
}

export default ({ match }) => <Play tournamentId={match.params.tournamentId} />;
