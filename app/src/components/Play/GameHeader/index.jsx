import React from 'react';
import Countdown from 'react-countdown-now';
import InlineSVG from 'svg-inline-react';
import { Link } from 'react-router-dom';
import { gameTitles } from '../../../app-core/appConfig';

import arrowLeftIcon from '../assets/arrow-left.svg';
import cogIcon from './cog.svg';
import './game-header.scss';

const goBack = () => {
  if (document.referrer === window.location.href) {
    const locationParts = window.location.href.split('/');
    locationParts.splice(locationParts.length - 2, 2);
    window.location.href = locationParts.join('/');
  } else {
    window.history.back();
  }
};

const GameHeader = props => (
  <div className="game-header game-header--container">
    <div className="game-header game-header--tournament">
      <div>
        <a className="game-header__control td-justify-flex-start" onClick={goBack}>
          <InlineSVG className="td-icon td-margin-side" src={arrowLeftIcon} />Back
        </a>
      </div>

      <Link to={`/settings/${props.tournamentId}`} className="game-header__control">
        {props.gameName}
      </Link>
      <Link to={`/settings/${props.tournamentId}`} className="game-header__control">
        <InlineSVG className="td-icon td-margin-side" src={cogIcon} />
      </Link>
    </div>
    <div className="game-header game-header--round">
      <div className="game-header__countdown">
        <Countdown date={props.nextTick} daysInHours />
      </div>
      <Link
        to={`/instructions/${props.gameType}`}
        className="game-header__round-name game-header__control"
      >
        {gameTitles[props.gameType]}
      </Link>
      <Link to={`/instructions/${props.gameType}`} className="game-header__control">
        <div className="game-header__help-icon">?</div>
      </Link>
    </div>
  </div>
);

export default GameHeader;
