import React from 'react';
import renderer from 'react-test-renderer';
import { StaticRouter, Route } from 'react-router';
import { player } from 'app-core';
import Play from './index';

const TOURNAMENT_ID = '1234';

jest.mock('app-core', () => ({
  player: {
    getTournament: jest.fn(TournamentId =>
      Promise.resolve({
        TournamentId,
        TournamentName: 'This is your game',
        CurrentRound: 1,
        NextTick: 60000,
        Players: ['me', 'you'],
        isAdmin: false,
        GameState: {
          typeId: 'commonsPot',
        },
      })),
    getUserId: jest.fn(() => 'userId'),
  },
}));

describe('Play component', () => {
  let component;
  beforeEach(() => {
    component = renderer.create(<StaticRouter context={{}} location={`/play/${TOURNAMENT_ID}`}>
        <Route path="/play/:tournamentId" component={Play} />
      </StaticRouter>);
  });
  afterEach(() => {
    player.getTournament.mockClear();
  });

  describe('snapshots', () => {
    test('onRender', () => expect(component.toJSON()).toMatchSnapshot());
  });

  describe('behaviours', () => {
    test("loads the user's active game data for the tournamentId", () => {
      expect(player.getTournament).toHaveBeenCalledWith(TOURNAMENT_ID);
    });
  });
});
