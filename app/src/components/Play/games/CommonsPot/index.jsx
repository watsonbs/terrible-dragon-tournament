import React from 'react';
import InlineSVG from 'svg-inline-react';
import Expander from '../../../elements/Expander';
import game from './service/game';
import tickIcon from './assets/tick.svg';
import arrowLeftIcon from '../../assets/arrow-left.svg';
import './commonsPot.scss';

class BeanCounters extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.state = {
      transferAmount: 0,
      previousResults: [
        {
          red: null,
          blue: 3,
          winner: 'blue',
        },
        {
          red: 2,
          blue: null,
          winner: 'blue',
        },
        {
          red: null,
          blue: 2,
          winner: 'red',
        },
        {
          red: null,
          blue: 2,
          winner: 'red',
        },
        {
          red: 4,
          blue: 2,
          winner: 'black',
        },
        {
          red: 3,
          blue: 3,
          winner: null,
        },
      ],
      me: {
        committedTokens: null,
        team: 'red',
        availableTokens: 8,
      },
      scores: {
        red: 2,
        blue: 2,
        black: 1,
      },
      players: {
        red: [
          { name: 'a-player', submitted: false },
          { name: 'b-player', submitted: true },
          { name: 'Constable wiggins', submitted: true },
          { name: 'maxy', submitted: true },
        ],
        blue: [
          { name: 'c-player', submitted: true },
          { name: 'Ratt', submitted: true },
          { name: 'Doctor hockley', submitted: true },
          { name: 'You', submitted: false },
        ],
        black: { name: 'black-player', team: 'black', submitted: true },
      },
    };

    this.updateTransferAmount = this.updateTransferAmount.bind(this);
    this.submitTransferAmount = this.submitTransferAmount.bind(this);
  }

  renderPlayer(player) {
    return (
      <li key={player.name} className="teams__player">
        <div className="teams__player-name">{player.name}</div>
        <div className="teams__submitted-icon">
          {player.submitted ? <InlineSVG src={tickIcon} /> : null}
        </div>
      </li>
    );
  }

  renderTeams(players) {
    return (
      <Expander title="Teams" titleClass="td-title" defaultExpanded>
        <div className="teams">
          <div className="td-card teams__team team-marked--red">
            <h3 className="td-title">Red</h3>
            <ul className="teams__team-members">{players.red.map(this.renderPlayer)}</ul>
          </div>
          <div className="td-card teams__team team-marked--blue">
            <h3 className="td-title">Blue</h3>
            <ul className="teams__team-members">{players.blue.map(this.renderPlayer)}</ul>
          </div>
        </div>
        {players.black ? (
          <div className="td-card teams__team teams__team--black team-marked--black">
            <h3 className="td-title">Black</h3>
            {this.renderPlayer(players.black)}
          </div>
        ) : null}
      </Expander>
    );
  }

  submitTransferAmount(event) {
    event.preventDefault();
    game.commitTokens(this.state.transferAmount);
  }
  updateTransferAmount(event) {
    const transferAmount = parseInt(event.target.value, 0);
    if (!transferAmount) {
      this.setState({ transferAmount: '' });
      return;
    }
    if (transferAmount <= this.state.me.availableTokens && transferAmount > 0) {
      this.setState({ transferAmount });
    }
  }
  renderPlayerStatus(player) {
    return (
      <Expander title="This Round" titleClass="td-title" defaultExpanded>
        <div className={`td-card team-marked--${player.team}`}>
          <div className="player-status">
            <div className="player-status__description">
              <h3 className="td-title">Your Tokens</h3>
              <div className="player-status__token-count">{player.availableTokens}</div>
            </div>
            <form
              className="td-form td-form--narrow player-status__controls"
              onSubmit={this.submitTransferAmount}
            >
              <div>Add tokens</div>
              <div
                className={`player-status__transfer-arrow player-status__transfer-arrow--${
                  player.team
                }`}
              >
                <InlineSVG src={arrowLeftIcon} />
              </div>
              <input
                className="td-input player-status__transfer-input"
                type="number"
                value={this.state.transferAmount}
                min="0"
                max={player.availableTokens}
                onChange={this.updateTransferAmount}
              />
              <button className="td-btn td-wide">Submit</button>
            </form>
            <div className="player-player-status__description">
              <h3 className="td-title">Committed Tokens</h3>
              <div className="player-status__token-count">
                {player.committedTokens || player.committedTokens === 0
                  ? player.committedTokens
                  : '...'}
              </div>
            </div>
          </div>
        </div>
      </Expander>
    );
  }

  renderPreviousResults(previousResults) {
    return (
      <Expander title="Previous Rounds" titleClass="td-title" defaultExpanded>
        <ul className="results">
          {previousResults.map((result, index) => (
            <li
              key={index}
              className={`td-card results__result team-marked--${result.winner || 'blank'}`}
            >
              <div className="results__winner">
                {result.winner ? `${result.winner} won` : 'draw'}
              </div>
              <div className="results__team-name">Red</div>
              <div>{result.red || '?'}</div>
              <div className="results__team-name">Blue</div>
              <div>{result.blue || '?'}</div>
            </li>
          ))}
        </ul>
      </Expander>
    );
  }

  render() {
    return (
      <div>
        {this.renderPreviousResults(this.state.previousResults)}
        {this.renderPlayerStatus(this.state.me)}
        {this.renderTeams(this.state.players)}
      </div>
    );
  }
}

export default BeanCounters;
