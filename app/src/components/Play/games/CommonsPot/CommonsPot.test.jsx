import React from 'react';
import renderer from 'react-test-renderer';
import CommonsPot from '.';
import { commitTokens } from './service/game';

const mockGame = {
  tournament: {
    tournamentId: '1234',
    tournamentTitle: 'This is your game',
    round: 1,
    nextTick: Date.now() + 60000,
  },
  gameType: {
    typeId: 'commonsPot',
    typeTitle: 'Bean Counters',
  },
  playerState: {
    chips: 10,
    team: 'red',
  },
  gameState: {
    round: 3,
    wins: {
      red: 1,
      blue: 1,
      black: 0,
    },
  },
};

jest.mock('./service/game', () => ({
  commitTokens: jest.fn(),
}));

describe('CommonsPot component', () => {
  let component;
  beforeEach(() => {
    component = renderer.create(<CommonsPot game={mockGame} />);
  });
  afterEach(() => {});

  test('can commit tokens', () => {
    const form = component.root.findByType('form');
    const input = component.root.findByType('input');
    const submitEvent = {
      preventDefault: jest.fn(),
    };
    input.props.onChange({ target: { value: 4 } });
    form.props.onSubmit(submitEvent);
    expect(submitEvent.preventDefault).toHaveBeenCalled();
    expect(commitTokens).toHaveBeenCalledWith(4);
  });

  describe('snapshots', () => {
    test('onRender', () => expect(component.toJSON()).toMatchSnapshot());
  });

  describe('behaviours', () => {});
});
