import React from 'react';
import renderer from 'react-test-renderer';
import Pending from './index';
import { tournaments } from 'app-core';

jest.mock('app-core', () => ({
  tournaments: {
    startTournament: jest.fn(() => Promise.resolve({ typeId: 'mock-game' })),
  },
}));

describe('Pending Game Component', () => {
  const mockTournament = {
    TournamentId: 'tournamentId',
    TournamentName: 'Tournament Name',
    Players: ['me', 'you'],
    TickLength: 60,
    RoundCount: 1,
  };
  const updateTournament = jest.fn();
  let component;
  beforeEach(() => {
    component = renderer.create(<Pending tournament={mockTournament} isAdmin updateTournament={updateTournament} />);
  });
  test('clicking the Start game button starts the game', (done) => {
    const startButton = component.root.findByType('button');
    startButton.props.onClick();
    expect(tournaments.startTournament).toHaveBeenCalledWith('tournamentId');
    setImmediate(() => {
      expect(updateTournament).toHaveBeenCalledWith({ typeId: 'mock-game' });
      done();
    });
  });
});
