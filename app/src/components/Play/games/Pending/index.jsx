import React from 'react';
import { tournaments } from 'app-core';

class Pending extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.startGame = this.startGame.bind(this);
  }
  startGame() {
    tournaments.startTournament(this.props.tournament.TournamentId).then((tournamentState) => {
      this.props.updateTournament(tournamentState);
    });
  }

  render() {
    const {
      TournamentName, TournamentId, TickLength, RoundCount, Players,
    } = this.props.tournament;
    return (
      <div>
        <h2 className="td-title">{TournamentName}</h2>
        <h3>Tournament Code</h3>
        <p>Share this code with other players you would like to invite to join</p>
        <p>{TournamentId}</p>
        <h3>Tournament Details</h3>
        <div>
          <div>Rounds</div>
          <div>{RoundCount}</div>
          <div>Tick Length</div>
          <div>{TickLength}</div>
        </div>
        <h3>Players</h3>
        <ul>{Players.map(player => <li key={player}>{player}</li>)}</ul>
        {this.props.isAdmin ? (
          <button onClick={this.startGame} className="td-btn">
            Start Game
          </button>
        ) : null}
      </div>
    );
  }
}

export default Pending;
