import React from 'react';

class TournamentSettings extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return <h2>TournamentSettings {this.props.tournamentId}</h2>;
  }
}

export default ({ match }) => <TournamentSettings tournamentId={match.params.tournamentId} />;
