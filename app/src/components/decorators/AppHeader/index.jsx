import React from 'react';
import { Link } from 'react-router-dom';
import { authentication } from 'td-core';
import './AppHeader.scss';

const AppHeader = ({ userSession }) => (
  <header className="td-justify-space-between app-header td-pad-sides-2">
    <a className="td-align-center header__logo-text" href="https://terribledragon.com">
      <img className="header__logo" src="/images/td-logo.svg" />
      <div className="td-margin-all">
        <div>Terrible</div>
        <div>Dragon</div>
      </div>
    </a>
    {userSession ? (
      <div className="td-align-center">
        <Link to="/account" className="td-align-center td-margin-side">
          <img className="td-border-soft-encircle" src="/images/icons/user.svg" />
          <div className="td-margin-side">{userSession['cognito:username']}</div>
        </Link>
        <button className="td-btn" onClick={authentication.logout}>
          Sign out
        </button>
      </div>
    ) : (
      <div className="td-align-center">
        <button onClick={authentication.login} className="td-btn">
          Sign in
        </button>
      </div>
    )}
  </header>
);

export default AppHeader;
