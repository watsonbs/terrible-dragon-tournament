import React, { Component } from 'react';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import { maxLength, required } from '../../utilities/validators';
import { tournaments } from 'app-core';
import { withRouter } from 'react-router-dom';

const DEFAULT_TICK_LENGTH = 28800;
class GameCreator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: { tickLength: DEFAULT_TICK_LENGTH },
      messages: {},
    };

    this.handleFormInput = this.handleFormInput.bind(this);
    this.showMessage = this.showMessage.bind(this);
    this.toggleTickLengthInfo = this.toggleTickLengthInfo.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }

  showMessage(key, text) {
    const messages = Object.assign({}, this.state.messages, { [key]: text });
    this.setState({ messages });
  }
  toggleTickLengthInfo() {
    this.showMessage(
      'tickLength',
      this.state.messages.tickLength
        ? null
        : 'This is the slowest the game will progress and should be long enough for all players to submit turns',
    );
  }
  submitForm(event) {
    event.preventDefault();
    const { gameName, tickLength, roundCount } = this.state.form;
    if (gameName && gameName.length <= 32 && tickLength && tickLength >= 60 && roundCount) {
      this.setState({ isSubmitting: true });
      tournaments.createTournament(this.state.form).then((tournament) => {
        this.props.close(event);
        this.props.history.push(`/play/${tournament.TournamentId}`);
      });
    } else {
      this.form.validateAll();
      this.setState({ submissionError: true });
      setTimeout(() => this.setState({ submissionError: false }), 1000);
    }
  }
  handleFormInput(event) {
    const form = Object.assign({}, this.state.form);
    form[event.target.name] = event.target.value;
    this.setState({ form });
  }
  render() {
    return (
      <div>
        <h2 className="td-title">Create New Game</h2>
        <Form
          ref={(c) => {
            this.form = c;
          }}
          className="td-form"
          onSubmit={this.submitForm}
        >
          <label className="td-control-label" htmlFor="gamecreator-gameName">
            Game Name
          </label>
          <Input
            className="td-input"
            onChange={this.handleFormInput}
            id="gamecreator-gameName"
            name="gameName"
            maxLength={32}
            validations={[required, maxLength]}
          />

          <div className="td-align-center">
            <label className="td-control-label" htmlFor="gamecreator-tickLength">
              Tick length
            </label>
            <img src="/images/icons/info.svg" onClick={this.toggleTickLengthInfo} />
          </div>
          <select
            className="td-select"
            onChange={this.handleFormInput}
            id="gamecreator-tickLength"
            name="tickLength"
            defaultValue={DEFAULT_TICK_LENGTH}
          >
            <option value="60">1 minute</option>
            <option value="3600">1 hour</option>
            <option value="14400">4 hours</option>
            <option value="28800">8 hours</option>
            <option value="43200">12 hours</option>
            <option value="86400">24 hours</option>
          </select>
          {this.state.messages.tickLength ? (
            <div className="td-form__message">{this.state.messages.tickLength}</div>
          ) : null}

          <label className="td-control-label" htmlFor="gamecreator-roundCount">
            Number of rounds
          </label>
          <Input
            className="td-input"
            id="gamecreator-roundCount"
            onChange={this.handleFormInput}
            name="roundCount"
            type="number"
            min="1"
            validations={[required]}
            disabled={this.isSubmitting}
          />

          <button
            type="submit"
            className={`td-btn td-wide td-form__submit ${
              this.state.submissionError ? 'animated shake' : ''
            }`}
          >
            Create game
          </button>
        </Form>
        <button onClick={this.props.close} className="td-modal__close-button" />
      </div>
    );
  }
}

export default withRouter(GameCreator);
