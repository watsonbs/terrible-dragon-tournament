import React from 'react';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import { required } from '../../utilities/validators';
import { tournaments } from 'app-core';
import { withRouter } from 'react-router-dom';

class GameJoiner extends React.Component {
  constructor() {
    super();
    this.state = { form: {} };

    this.submitForm = this.submitForm.bind(this);
    this.handleFormInput = this.handleFormInput.bind(this);
  }
  submitForm(event) {
    event.preventDefault();
    const { gameId } = this.state.form;
    if (gameId) {
      this.setState({ isSubmitting: true });
      tournaments.joinTournament(gameId).then(() => {
        this.props.close(event);
        this.props.history.push(`/play/${gameId}`);
      });
    } else {
      this.form.validateAll();
      this.setState({ submissionError: true });
      setTimeout(() => this.setState({ submissionError: false }), 1000);
    }
  }
  handleFormInput(event) {
    const form = Object.assign({}, this.state.form);
    form[event.target.name] = event.target.value;
    this.setState({ form });
  }
  render() {
    return (
      <div>
        <h2 className="td-title">Join Game</h2>
        <p>
          A proper way of joining games is coming. For now, the game creator will receive a long
          numerical id to share with you. Put that in here and submit to join the game.
        </p>
        <Form
          ref={(c) => {
            this.form = c;
          }}
          className="td-form"
          onSubmit={this.submitForm}
        >
          <label className="td-control-label" htmlFor="gamejoiner-gameId">
            Game Id
          </label>
          <Input
            className="td-input"
            onChange={this.handleFormInput}
            id="gamejoiner-gameId"
            name="gameId"
            validations={[required]}
          />
          <button
            type="submit"
            className={`td-btn td-wide td-form__submit ${
              this.state.submissionError ? 'animated shake' : ''
            }`}
            disabled={this.state.isSubmitting}
          >
            Join game
          </button>
        </Form>
        <button onClick={this.props.close} className="td-modal__close-button" />
      </div>
    );
  }
}
export default withRouter(GameJoiner);
