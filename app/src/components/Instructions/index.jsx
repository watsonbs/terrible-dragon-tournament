import React from 'react';

const loadComponent = pageId => import(`./pages/${pageId}`);

class Instructions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pages: {},
    };

    this.activatePage(this.props.pageId);
  }
  activatePage(pageId) {
    if (!this.state.pages[pageId]) {
      loadComponent(pageId).then(module => {
        const pages = { ...this.state.pages, [pageId]: module.default };
        this.setState({ pages, active: pageId });
      });
    } else {
      this.setState({ active: pageId });
    }
  }
  componentDidUpdate() {
    if (this.state.active !== this.props.pageId) {
      this.activatePage(this.props.pageId);
    }
  }
  render() {
    const Component = this.state.pages[this.state.active];
    return <div>{Component ? <Component active={this.state.active} /> : 'Loading...'}</div>;
  }
}

export default ({ match }) => <Instructions pageId={match.params.pageId} />;
