import React from 'react';
import { Link } from 'react-router-dom';

export default () => (
  <div>
    <h1>Terrible Dragon Tournament</h1>
    <p>
      A Terrible Dragon Tournament involves a series of separate games played by the same group of
      people over the course of several days.
    </p>
    <h2>Playing the tournament</h2>
    <p>
      When the tournament is set up it will have a fixed set of players and number of rounds. Each
      round will be a different randomly selected game from the{' '}
      <Link to="/instructions/catalogue">Terrible Dragon Catalogue</Link>
    </p>
    <p>
      Some rounds will be solo games and others team games, but there will always be an overall
      winner selected and an overall loser. The winner will gain Tournament Points and the loser
      will lose them. You will need to work with other players, whether or not they are on your team
      in the current game, to try and increase your overall score.
    </p>
    <h2>Ending the tournament</h2>
    <p>
      The tournament ends after all rounds have been played. The winner is the player with the most
      Tournament Points at the end.
    </p>
  </div>
);
