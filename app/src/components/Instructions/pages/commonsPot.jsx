import React from 'react';
import { Link } from 'react-router-dom';
import { gameTitles } from '../../../app-core/appConfig';

const points = {
  first: 200,
  second: 100,
  team: 10,
  loser: 50,
};

console.log(gameTitles);

export default () => (
  <div>
    <h1>{gameTitles.commonsPot}</h1>
    <p>
      {gameTitles.commonsPot} is a round in the{' '}
      <Link to="/instructions/tournament">Terrible Dragon Tournament</Link>.
    </p>
    <h2>Playing the game</h2>
    <p>
      You will be given 10 tokens and randomly be sorted into two equal teams, Red and Blue. There
      will also be a special <a href="#odd-players">Black team</a> if there is an odd number of
      players.
    </p>
    <p>
      Each round you will be able to put as many or as few tokens as you like from your supply into
      the central pot. The team with the most tokens in the pot at the end of the round win a point.
    </p>
    <p>
      After a round is over the number of tokens contributed by the winning team is not revealed,
      but the losing team is. Individual contributions by players are never revealed.
    </p>
    <p>
      If both teams enter an equal number of tokens then neither team is the winner and the amount
      committed is revealed for both teams. All players will then receive an additional token.
    </p>
    <h2>Ending the game</h2>
    <p>The first team to reach 3 points wins the game.</p>
    <p>
      At the end of the game, every player on the winning team gets {points.team} Tournament Points.
      However, the player with the most unused tokens on the winning team is the overall winner. The
      overall winner scores {points.first} Tournament Points and can nominate any player on any team
      other than the overall loser as second place, who will receive {points.second} Tournament
      Points.
    </p>
    <p>
      The overall loser is the player on the losing team with the least unused tokens, they will
      lose {points.loser} Tournament Points.
    </p>
    <h3>Ties</h3>
    <p>
      If there is a tie for the winner, the winning team will vote for the winner. If there is still
      a tie the winner will be randomly chosen between the candidates.
    </p>
    <p>If there is a tie for the loser, the winner will choose between the candidates</p>

    <h2 id="odd-players">Odd Player Counts</h2>
    <p>If you have an odd number of players, one player alone becomes the Black Team.</p>
    <p>
      The Black Team does not submit tokens to the pot. Instead, they try to predict the number of
      tokens submitted by the team winning the round. If they get the correct number then Black Team
      wins the round instead. If the Red and Blue teams tie on the number predicted then the Black
      Team still wins the round.
    </p>
    <p>
      When the Black Team wins a round, the tokens submitted by both teams are revealed to everyone
      and they score a point.
    </p>
    <p>
      The Black Team wins if they reach 3 points and that player becomes the overall winner, scoring{' '}
      {points.winner} Tournament Points and able to nominate a second place to win {points.second}{' '}
      Tournament Points. When the Black Team wins, both the Red and Blue teams are considered to be
      the losing teams for determining the overall loser. The Black Team player will never be the
      overall loser.
    </p>
    <h2>Tips</h2>
    <p>
      Although you are in teams, most of the points available are for you as an individual. You will
      need to balance helping the team with protecting your own interests.
    </p>
    <p>
      Working with members of the other teams can be a good ideas. You'll score more points if your
      team loses and you get nominated as Second Place than if your team wins but someone else takes
      the overall winner position.
    </p>
    <h2>Technical Details</h2>
    <p>You cannot take tokens back after you have comitted them to the pot.</p>
    <p>
      When you commit any number of tokens to the pot, even if that number is 0, you are marked as
      submitted.
    </p>
  </div>
);
