import React from 'react';
import { Link } from 'react-router-dom';

export default () => (
  <div>
    <h1>Terrible Dragon Game Catalogue</h1>
    <Link to="/instructions/commons-pot">Commons Pot (Working title)</Link>
  </div>
);
