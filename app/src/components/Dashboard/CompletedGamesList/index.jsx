import React from 'react';

export default ({ games }) => (
  <div className="td-card dashboard__card">
    <h2 className="td-title">Completed Games</h2>
    <ul className="dashboard__list">
      {games.map(game => <li key={game.tournamentId}>{game.tournamentTitle}</li>)}
    </ul>
  </div>
);
