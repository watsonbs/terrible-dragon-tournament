import React from 'react';
import { Link } from 'react-router-dom';
import Countdown from 'react-countdown-now';
import InlineSVG from 'svg-inline-react';
import tickIcon from './tick.svg';

export default ({ games }) => (
  <div className="td-card dashboard__card">
    <h2 className="td-title">Active Games</h2>
    <ul className="dashboard__list">
      {games.map(game => (
        <li className="dashboard__list-item td-justify-flex-start" key={game.TournamentId}>
          <Link className="td-btn td-wide" to={`/play/${game.TournamentId}`}>
            {game.TournamentName}
          </Link>
          <div className="dashboard__clock td-align-center td-margin-side">
            {game.NextTick ? <Countdown date={game.NextTick} daysInHours /> : ''}
          </div>
          <div className="dashboard__submitted-icon">
            {game.Submitted ? <InlineSVG src={tickIcon} /> : null}
          </div>
        </li>
      ))}
    </ul>
  </div>
);
