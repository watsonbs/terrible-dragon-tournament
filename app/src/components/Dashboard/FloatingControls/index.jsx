import React from 'react';
import ReactModal from 'react-modal';
import InlineSVG from 'svg-inline-react';
import addIcon from './add.svg';
import addUserIcon from './add-user.svg';
import GameCreator from '../../GameCreator';
import GameJoiner from '../../GameJoiner';

class CreateGameControl extends React.Component {
  constructor() {
    super();
    this.state = { showModal: false };

    this.closeModal = this.closeModal.bind(this);
  }

  closeModal(event, response) {
    if (response && this.props.callback) {
      this.props.callback(response);
    }
    this.setState({ showModal: false });
  }

  render() {
    return (
      <div>
        {this.state.showModal ? (
          <ReactModal
            isOpen={Boolean(this.state.showModal)}
            contentLabel="Create New Game"
            onRequestClose={this.closeModal}
            shouldCloseOnOverlayClick
            className="td-modal animated bounceInUp"
          >
            {this.state.showModal === 'join' ? (
              <GameJoiner close={this.closeModal} />
            ) : (
              <GameCreator close={this.closeModal} />
            )}
          </ReactModal>
        ) : (
          <div className="td-FAB-container">
            <div className="td-FAB">
              <label className="td-FAB__label">Join Game</label>
              <button
                className="td-FAB__button td-FAB__button--secondary"
                onClick={() => this.setState({ showModal: 'join' })}
              >
                <div className="td-FAB__button-icon">
                  <InlineSVG src={addUserIcon} />
                </div>
              </button>
            </div>
            <div className="td-FAB">
              <label className="td-FAB__label">Create Game</label>
              <button
                className="td-FAB__button"
                onClick={() => this.setState({ showModal: 'create' })}
              >
                <div className="td-FAB__button-icon">
                  <InlineSVG src={addIcon} />
                </div>
              </button>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default CreateGameControl;
