import React, { Component } from 'react';
import { player } from 'app-core';
import './Dashboard.scss';
import ActiveGamesList from './ActiveGamesList';
import FloatingControls from './FloatingControls';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    player.getTournaments().then(tournaments => this.setState({ tournaments }));
    this.refreshTournaments = this.refreshTournaments.bind(this);
  }
  refreshTournaments() {
    player.getTournaments().then(tournaments => this.setState({ tournaments }));
  }

  render() {
    return (
      <div>
        {this.state.tournaments ? (
          <ActiveGamesList games={this.state.tournaments} />
        ) : (
          <div>loading...</div>
        )}
        <FloatingControls callback={this.refreshTournaments} />
      </div>
    );
  }
}

export default Dashboard;
