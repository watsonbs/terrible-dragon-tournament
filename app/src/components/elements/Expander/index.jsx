import React from 'react';
import './expander.scss';

class Expander extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isExpanded: props.defaultExpanded,
    };
  }

  render() {
    return (
      <div>
        <button
          className="expander-button"
          onClick={() => this.setState({ isExpanded: !this.state.isExpanded })}
        >
          <div className="expander-icon">{this.state.isExpanded ? '+' : '-'}</div>
          <h3 className={this.props.titleClass}>{this.props.title}</h3>
        </button>
        {this.state.isExpanded ? this.props.children : null}
      </div>
    );
  }
}

export default Expander;
