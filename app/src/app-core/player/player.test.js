import { getTournaments, getTournament } from './index';
import { dragonGet } from '../dragon';

jest.mock('../dragon', () => ({
  dragonGet: jest.fn(() => 'mockGetResp'),
}));

describe('player', () => {
  const mockGetResp = 'mockGetResp';

  test('can get the list of tournaments for the player', () => {
    expect(getTournaments()).toEqual(mockGetResp);
    expect(dragonGet).toHaveBeenCalledWith('/me/tournaments');
  });

  test('can get a single tournament for the player', () => {
    const tournamentId = 'tournamentId';
    expect(getTournament(tournamentId)).toEqual(mockGetResp);
    expect(dragonGet).toHaveBeenCalledWith(`/me/tournaments/${tournamentId}`);
  });
});
