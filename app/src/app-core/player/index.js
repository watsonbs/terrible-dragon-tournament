import { dragonGet } from '../dragon';
import authentication from '../../td-core/authentication';

export const getTournaments = () => dragonGet('/me/tournaments');

export const getTournament = tournamentId => dragonGet(`/me/tournaments/${tournamentId}`);

export const getUserId = () => authentication.getSession()['cognito:username'];

export default {
  getTournaments,
  getTournament,
  getUserId,
};
