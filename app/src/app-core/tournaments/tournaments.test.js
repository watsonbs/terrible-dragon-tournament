import { createTournament, joinTournament } from './index';
import { dragonPost } from '../dragon';

jest.mock('../dragon', () => ({
  dragonPost: jest.fn(() => 'mockPostResp'),
}));

describe('tournaments', () => {
  const mockPostResp = 'mockPostResp';

  test('can create new games', () => {
    const tournamentOptions = {
      roundCount: 2,
      gameName: 'My Game',
      tickLength: 60,
    };

    expect(createTournament(tournamentOptions)).toEqual(mockPostResp);
    expect(dragonPost).toHaveBeenCalledWith('/tournament/create', tournamentOptions);
  });

  test('can join games', () => {
    const tournamentId = 'tournamentId';
    expect(joinTournament(tournamentId)).toEqual(mockPostResp);
    expect(dragonPost).toHaveBeenCalledWith(`/tournament/join/${tournamentId}`, undefined);
  });
});
