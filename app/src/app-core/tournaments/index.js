import { dragonPost } from '../dragon';

export const createTournament = tournamentOptions =>
  dragonPost('/tournament/create', tournamentOptions);

export const joinTournament = (tournamentId, body) =>
  dragonPost(`/tournament/join/${tournamentId}`, body);

export const startTournament = tournamentId =>
  dragonPost(`/tournament/start/${tournamentId}`).then(response => response.tournament);

export default { createTournament, joinTournament, startTournament };
