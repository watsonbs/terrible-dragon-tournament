import { authentication } from '../../td-core/td-core';
import appConfig from '../appConfig';

const dragonFetch = (path, opts) => {
  const url = `${appConfig.apiRoot}${path}`;
  const requestOptions = {
    ...opts,
    headers: { Authorization: authentication.getToken(), ...opts.headers },
  };

  return fetch(url, requestOptions).then(resp => resp.json());
};
export const dragonPost = (path, body, opts) =>
  dragonFetch(path, { ...opts, body: JSON.stringify(body), method: 'POST' });
export const dragonGet = (path, opts) => dragonFetch(path, { ...opts, method: 'GET' });

export default { dragonPost, dragonGet };
