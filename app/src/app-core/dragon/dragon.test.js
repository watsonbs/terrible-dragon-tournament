import { dragonPost, dragonGet } from './index';

jest.mock('../appConfig', () => ({
  apiRoot: 'apiRoot',
}));

jest.mock('td-core', () => ({
  authentication: {
    getToken: jest.fn(() => 'mock-token'),
  },
}));

describe('dragon', () => {
  const realFetch = global.fetch;
  const headers = {
    Authorization: 'mock-token',
  };
  const body = { myBody: true };
  const mockFetchResponse = Promise.resolve({ json: () => 'mockFetchResponse' });
  beforeEach(() => {
    global.fetch = jest.fn(() => mockFetchResponse);
  });
  afterEach(() => {
    global.fetch = realFetch;
  });

  describe('dragonPost', () => {
    beforeEach(() => {
      expect(dragonPost('/my/path', body)).toEqual(mockFetchResponse);
    });
    test('adds the apiRoot to requests', () =>
      expect(fetch).toHaveBeenCalledWith('apiRoot/my/path', expect.any(Object)));

    test('adds the token as the Authorization header to requests', () =>
      expect(fetch).toHaveBeenCalledWith(expect.any(String), {
        body: JSON.stringify(body),
        headers,
        method: 'POST',
      }));

    test('includes the body in the requst', () =>
      expect(fetch).toHaveBeenCalledWith(expect.any(String), {
        body: JSON.stringify(body),
        headers,
        method: 'POST',
      }));

    test('sets the method to POST', () =>
      expect(fetch).toHaveBeenCalledWith(expect.any(String), {
        body: JSON.stringify(body),
        headers,
        method: 'POST',
      }));

    test('includes request options if they are included', () => {
      expect(dragonPost('/my/path', body, { redirect: 'follow' })).toEqual(mockFetchResponse);
      expect(fetch).toHaveBeenCalledWith(expect.any(String), {
        body: JSON.stringify(body),
        headers,
        method: 'POST',
        redirect: 'follow',
      });
    });
  });

  describe('dragonGet', () => {
    beforeEach(() => {
      expect(dragonGet('/my/path')).toEqual(mockFetchResponse);
    });
    test('adds the apiRoot to requests', () =>
      expect(fetch).toHaveBeenCalledWith('apiRoot/my/path', expect.any(Object)));

    test('adds the token as the Authorization header to requests', () =>
      expect(fetch).toHaveBeenCalledWith(expect.any(String), {
        headers,
        method: 'GET',
      }));

    test('includes the body in the requst', () =>
      expect(fetch).toHaveBeenCalledWith(expect.any(String), {
        headers,
        method: 'GET',
      }));

    test('sets the method to GET', () =>
      expect(fetch).toHaveBeenCalledWith(expect.any(String), {
        headers,
        method: 'GET',
      }));

    test('includes request options if they are included', () => {
      expect(dragonGet('/my/path', { redirect: 'follow' })).toEqual(mockFetchResponse);
      expect(fetch).toHaveBeenCalledWith(expect.any(String), {
        headers,
        method: 'GET',
        redirect: 'follow',
      });
    });
  });
});
