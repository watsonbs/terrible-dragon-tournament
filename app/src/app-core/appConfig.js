const environment = 'Stage';

export const gameTitles = {
  commonsPot: 'Commons Pot (Working title)',
};

export default {
  apiRoot: `https://api.terribledragon.com/${environment}`,
  gameTitles,
};
