import authentication from './authentication';
import { getHashVariables } from './hashVariables';

jest.mock('./hashVariables', () => ({
  getHashVariables: jest.fn(),
}));

describe('authentication', () => {
  const validToken =
    'eyJraWQiOiJaU0M1UmsrWVMrL3k2NWZLeGZQbFNaSGcvN0M1ZDV5c3FUdGdwY2dQVXJJPSIsImFsZyI6IkhTNTEyIn0.eyJhdF9oYXNoIjoiQWNwdzRLM0FBazRfYlBZVFFUQ0N4dyIsInN1YiI6ImYxNTI1YTRjLTc3NGYtNDYxOS1iYzFhLWZmNWM0N2Q0MDg4OSIsImF1ZCI6IjF2bGRoMjg5MnZhN2MwdWFmdWdxNGplM2dvIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImV2ZW50X2lkIjoiYTdkMmI2MjUtMzQzNy0xMWU4LTlhZTAtZDkyZjZkMmZhN2U5IiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE1MjI0Mjc0MjgsImlzcyI6Imh0dHBzOi8vY29nbml0by1pZHAuZXUtd2VzdC0xLmFtYXpvbmF3cy5jb20vZXUtd2VzdC0xX0FZbWsySUNseCIsImNvZ25pdG86dXNlcm5hbWUiOiJ0ZXN0IiwiZXhwIjoxNTIyNDMxMDI4LCJpYXQiOjE1MjI0Mjc0MjgsImVtYWlsIjoidGVzdEBlbWFpbC5jb20ifQ.HjyMjhgTBd4gBQP7BGa4KE2F9eUdIgz5Sv5H39R_Y9_7uIfrrP_tpz3YH_cyZWbOPIXRvmk_I-B_4JAfFugX_A';
  const validTokenSession = {
    at_hash: 'Acpw4K3AAk4_bPYTQTCCxw',
    sub: 'f1525a4c-774f-4619-bc1a-ff5c47d40889',
    aud: '1vldh2892va7c0uafugq4je3go',
    email_verified: true,
    event_id: 'a7d2b625-3437-11e8-9ae0-d92f6d2fa7e9',
    token_use: 'id',
    auth_time: 1522427428,
    iss: 'https://cognito-idp.eu-west-1.amazonaws.com/eu-west-1_AYmk2IClx',
    'cognito:username': 'test',
    exp: 1522431028,
    iat: 1522427428,
    email: 'test@email.com',
  };
  const expiredToken =
    'eyJraWQiOiJaU0M1UmsrWVMrL3k2NWZLeGZQbFNaSGcvN0M1ZDV5c3FUdGdwY2dQVXJJPSIsImFsZyI6IkhTNTEyIn0.eyJhdF9oYXNoIjoiQWNwdzRLM0FBazRfYlBZVFFUQ0N4dyIsInN1YiI6ImYxNTI1YTRjLTc3NGYtNDYxOS1iYzFhLWZmNWM0N2Q0MDg4OSIsImF1ZCI6IjF2bGRoMjg5MnZhN2MwdWFmdWdxNGplM2dvIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImV2ZW50X2lkIjoiYTdkMmI2MjUtMzQzNy0xMWU4LTlhZTAtZDkyZjZkMmZhN2U5IiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE0MjI0Mjc0MjgsImlzcyI6Imh0dHBzOi8vY29nbml0by1pZHAuZXUtd2VzdC0xLmFtYXpvbmF3cy5jb20vZXUtd2VzdC0xX0FZbWsySUNseCIsImNvZ25pdG86dXNlcm5hbWUiOiJ0ZXN0IiwiZXhwIjoxNDIyNDMxMDI4LCJpYXQiOjE0MjI0Mjc0MjgsImVtYWlsIjoidGVzdEBlbWFpbC5jb20ifQ.1ms3YcxayTQNfbig9N4Bxjiez9XYAahhk-iSCXHVdc-Qb7BFfMkFadgcU3AOwW566tmLyaowEN3XYlp0rjHQjQ';
  const fakeTime = 1522431020000;

  const realLocalStorage = window.localStorage;
  const realDateNow = Date.now;
  let storage = {};

  beforeEach(() => {
    window.localStorage = {
      setItem: jest.fn((key, value) => {
        storage[key] = value;
      }),
      removeItem: jest.fn(),
      getItem: jest.fn(key => storage[key]),
    };
    Date.now = jest.fn(() => fakeTime);
  });
  afterEach(() => {
    window.location.hash = '';
    document.cookie = '';
    window.localStorage = realLocalStorage;
    storage = {};
    Date.now = realDateNow;
  });

  test('login redirects to the Cognito login page', () => {
    const realAssign = window.location.assign;
    window.location.assign = jest.fn();
    authentication.login();

    expect(window.location.assign).toHaveBeenCalledWith('https://terribledragon.auth.eu-west-1.amazoncognito.com/login?response_type=token&client_id=1vldh2892va7c0uafugq4je3go&redirect_uri=https%3A%2F%2Fterribledragon.com%2Ftournament');

    window.location.assign = realAssign;
  });
  test('logout removes the token and refreshes the page', () => {
    const realReload = window.location.reload;
    window.location.reload = jest.fn();

    authentication.logout();

    expect(window.localStorage.removeItem).toHaveBeenCalledWith('token');
    expect(window.location.reload).toHaveBeenCalled();

    window.location.reload = realReload;
  });

  describe('init', () => {
    test('success returns true', () => {
      getHashVariables.mockImplementation(() => ({ id_token: validToken }));
      expect(authentication.init()).toEqual(validTokenSession);
    });

    test('fail returns null', () => {
      getHashVariables.mockImplementation(() => ({}));
      expect(authentication.init()).toEqual(null);
    });

    test('retrieves the token from localStorage if there is no url token and one is present', () => {
      storage.token = validToken;
      getHashVariables.mockImplementation(() => ({}));
      authentication.init();
      expect(authentication.getToken()).toEqual(validToken);
    });

    test('prioritises url token over localStorage', () => {
      storage.token = expiredToken;
      getHashVariables.mockImplementation(() => ({ id_token: validToken }));
      authentication.init();
      expect(authentication.getToken()).toEqual(validToken);
    });

    test('discards expired tokens from localStorage', () => {
      getHashVariables.mockImplementation(() => ({}));
      storage.token = expiredToken;
      expect(authentication.init()).toEqual(null);
    });

    test('discards expired tokens from url', () => {
      getHashVariables.mockImplementation(() => ({ id_token: expiredToken }));
      expect(authentication.init()).toEqual(null);
    });

    describe('url token', () => {
      beforeEach(() => {
        getHashVariables.mockImplementation(() => ({ id_token: validToken }));
        authentication.init();
      });

      test('retrieves the token from the url hash', () =>
        expect(authentication.getToken()).toEqual(validToken));

      test('puts the token in localStorage', () =>
        expect(localStorage.getItem('token')).toEqual(validToken));

      test('removes the authentication hash', () => expect(window.location.hash).toEqual(''));

      test('can get session data', () =>
        expect(authentication.getSession()).toEqual(validTokenSession));

      test('cannot edit the session object', () => {
        const session = authentication.getSession();
        session.email = 'NEW EMAIL';
        expect(authentication.getSession().email).toEqual('test@email.com');
      });
    });
  });
});
