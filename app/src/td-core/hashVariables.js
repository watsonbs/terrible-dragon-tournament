let hashVariables;
export const getHashVariables = () => {
  if (!hashVariables) {
    const hashValue = window.location.hash.replace('#', '');
    const hashParts = hashValue.split('&');
    hashVariables = hashParts.reduce((pairs, part) => {
      const partParts = part.split('=');
      return { ...pairs, [partParts[0]]: partParts[1] };
    }, {});

    history.pushState('', document.title, window.location.pathname + window.location.search); // eslint-disable-line no-restricted-globals
  }

  return hashVariables;
};
export default {
  getHashVariables,
};
