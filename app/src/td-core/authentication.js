import { getHashVariables } from './hashVariables';

const RETURN_LINK = encodeURIComponent('https://terribledragon.com/tournament');
const CLIENT_ID = '1vldh2892va7c0uafugq4je3go';
const AUTH_LINK = `https://terribledragon.auth.eu-west-1.amazoncognito.com/login?response_type=token&client_id=${CLIENT_ID}&redirect_uri=${RETURN_LINK}`;

let token = '';
let session = null;

const decodeToken = () => {
  const base64Url = token.split('.')[1];
  if (!base64Url) return '';
  const base64 = base64Url.replace('-', '+').replace('_', '/');
  return JSON.parse(window.atob(base64));
};
const isSessionValid = () => {
  if (!session || !session.exp || Date.now() > session.exp * 1000) return false;
  return true;
};

const authentication = {
  init: () => {
    token = getHashVariables().id_token;
    if (!token) {
      token = localStorage.getItem('token');
    }
    if (!token) return null;

    session = decodeToken();

    if (!session) return null;

    if (!isSessionValid()) {
      return null;
    }

    localStorage.setItem('token', token);
    return session;
  },
  getToken: () => token,
  getSession: () => {
    if (!token) return null;

    return Object.assign({}, session);
  },
  login: () => {
    // localStorage.setItem(
    //   'token',
    // eslint-disable-next-line max-len
    //   'eyJraWQiOiJaU0M1UmsrWVMrL3k2NWZLeGZQbFNaSGcvN0M1ZDV5c3FUdGdwY2dQVXJJPSIsImFsZyI6IkhTNTEyIn0.eyJhdF9oYXNoIjoiNTkxUlFpTHB5Z0Y2OEFSLXlYYXpaUSIsInN1YiI6ImYxNTI1YTRjLTc3NGYtNDYxOS1iYzFhLWZmNWM0N2Q0MDg4OSIsImF1ZCI6IjF2bGRoMjg5MnZhN2MwdWFmdWdxNGplM2dvIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImV2ZW50X2lkIjoiODkxZGI5YmItMzRiZC0xMWU4LThhMmUtNmRjMThhZTk1NmQ5IiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE1MjI0ODQ5MjksImlzcyI6Imh0dHBzOi8vY29nbml0by1pZHAuZXUtd2VzdC0xLmFtYXpvbmF3cy5jb20vZXUtd2VzdC0xX0FZbWsySUNseCIsImNvZ25pdG86dXNlcm5hbWUiOiJhZG1pbiIsImV4cCI6MjUyMjQ4ODUyOSwiaWF0IjoxNTIyNDg0OTI5LCJlbWFpbCI6IndhdHNvbmJzQGdtYWlsLmNvbSJ9.ArIQPp1fUZAp8wtSRA1T-nMt7ecr95mX-bADPlKE55Chc8rKY3OKOf7j7ExNts6qinDorI9yNUsJ078r1afLGA',
    // );
    // location.reload();
    window.location.assign(AUTH_LINK);
  },
  logout: () => {
    localStorage.removeItem('token');
    location.reload(); // eslint-disable-line no-restricted-globals
  },
};

export default authentication;
