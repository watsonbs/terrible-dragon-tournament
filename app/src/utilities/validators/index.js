import maxLength from './maxLength';
import required from './required';

export { maxLength, required };
