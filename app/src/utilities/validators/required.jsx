import React from 'react';

export default (value) => {
  if (!value.toString().trim().length) {
    return <div className="td-form__message td-form__message--error">This field is required</div>;
  }
};
