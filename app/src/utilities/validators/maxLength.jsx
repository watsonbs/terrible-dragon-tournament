import React from 'react';

export default (value, props) => {
  if (!value.toString().trim().length > props.maxLength) {
    return (
      <div className="td-form__message td-form__message--error">
        Cannot be longer than {props.maxLength} characters
      </div>
    );
  }
};
