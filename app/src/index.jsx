import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import LazyRoute from 'react-lazy-route';
import { authentication } from 'td-core';
import AppHeader from './components/decorators/AppHeader';
import AppFooter from './components/decorators/AppFooter';
import Root from './components/Root';
import Dashboard from './components/Dashboard';
import ReactModal from 'react-modal';

const Account = () => import('./components/Account');
const Play = () => import('./components/Play');
const NotFound = () => import('./components/NotFound');
const Instructions = () => import('./components/Instructions');
const TournamentSettings = () => import('./components/TournamentSettings');

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      session: authentication.init(),
    };
  }

  render() {
    return (
      <Router>
        <div>
          <AppHeader userSession={this.state.session} />
          <section className="td-content">
            <Switch>
              <LazyRoute exact path="/" component={this.state.session ? Dashboard : Root} />
              <LazyRoute
                path="/account"
                restrict
                render={Account}
                allow={this.state.session}
                onForbidden="/"
              />
              <LazyRoute
                path="/play/:tournamentId"
                restrict
                render={Play}
                allow={this.state.session}
                onForbidden="/"
              />
              <LazyRoute path="/instructions/:pageId" render={Instructions} />
              <LazyRoute path="/settings/:tournamentId" render={TournamentSettings} />
              <LazyRoute path="*" render={NotFound} />
            </Switch>
          </section>
          <AppFooter />
        </div>
      </Router>
    );
  }
}

ReactModal.setAppElement('#app');
render(<App />, document.getElementById('app'));
