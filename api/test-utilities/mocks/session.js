module.exports = {
  at_hash: 'Acpw4K3AAk4_bPYTQTCCxw',
  sub: 'f1525a4c-774f-4619-bc1a-ff5c47d40889',
  aud: '1vldh2892va7c0uafugq4je3go',
  email_verified: true,
  event_id: 'a7d2b625-3437-11e8-9ae0-d92f6d2fa7e9',
  token_use: 'id',
  auth_time: 1522427428,
  iss: 'https://cognito-idp.eu-west-1.amazonaws.com/eu-west-1_AYmk2IClx',
  'cognito:username': 'test',
  exp: 1522431028,
  iat: 1522427428,
  email: 'test@email.com',
};
