module.exports = {
  ResourceId: 'gameId',
  TournamentName: 'My Game',
  RoundCount: 1,
  CurrentRound: 0,
  TickLength: 60,
  ExpiresAt: 1538470166857,
  Administrator: 'admin',
  Players: ['admin', 'test'],
};
