const tournamentController = require('../tournamentController');
const createResponse = require('../../utilities/createResponse');
const getUserId = require('../../utilities/getUserId');

exports.handler = (event, context, callback) => {
  const { pathParameters } = event;
  const userId = getUserId(event);
  const gameId = pathParameters.tournamentId;

  tournamentController
    .start(userId, gameId)
    .then(tournament => callback(null, createResponse({ ok: true, tournament })))
    .catch((error) => {
      const statusCode = error.statusCode || 400;
      callback(null, createResponse(error.message, statusCode));
    });
};
