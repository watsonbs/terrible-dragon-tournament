const startTournament = require('./startTournament');
const tournamentController = require('../tournamentController');
const mockEvent = require('../../../test-utilities/mocks/validRequestEvent');

tournamentController.start = jest.fn();
const context = {};

describe('startTournament lambda', () => {
  describe('successful requests', () => {
    beforeEach(() => {
      tournamentController.start.mockImplementation(() =>
        Promise.resolve({ gameType: 'mock-game' }));
    });

    test('starts the tournament and returns it if it permitted', (done) => {
      startTournament.handler(
        {
          requestContext: mockEvent.requestContext,
          pathParameters: { tournamentId: 'gameId' },
        },
        context,
        (error, response) => {
          expect(response).toEqual({
            body: JSON.stringify({ ok: true, tournament: { gameType: 'mock-game' } }),
            statusCode: 200,
            headers: {
              'Access-Control-Allow-Origin': '*',
            },
          });
          expect(tournamentController.start).toHaveBeenCalledWith('userId', 'gameId');
          done();
        },
      );
    });
  });

  describe('unsuccessful requests', () => {
    test('failed to start', (done) => {
      tournamentController.start.mockImplementationOnce(() => Promise.reject(new Error('biffed')));

      startTournament.handler(
        {
          requestContext: mockEvent.requestContext,
          pathParameters: { tournamentId: 'gameId' },
        },
        context,
        (error, response) => {
          expect(response).toEqual({
            statusCode: 400,
            body: 'biffed',
            headers: {
              'Access-Control-Allow-Origin': '*',
            },
          });
          done();
        },
      );
    });
  });
});
