const tournamentController = require('../tournamentController');
const createResponse = require('../../utilities/createResponse');
const logger = require('../../utilities/logger');
const getUserId = require('../../utilities/getUserId');
const errors = require('../../errors');

const MAX_NAME_LENGTH = 64;
const MIN_TICK_LENGTH = 30;
const EPOCH = 1300000000000;

const generateRowId = () => {
  let ts = Date.now() - EPOCH; // limit to recent
  const randid = Math.floor(Math.random() * 512);
  ts *= 64; // bit-shift << 6
  return (ts * 512 + randid % 512).toString(); // eslint-disable-line no-mixed-operators
};
const isValidRequest = ({ gameName, tickLength, roundCount }) =>
  gameName &&
  gameName.length <= MAX_NAME_LENGTH &&
  tickLength &&
  tickLength >= MIN_TICK_LENGTH &&
  roundCount;

exports.handler = (event, context, callback) => {
  const { body } = event;
  const userId = getUserId(event);
  const data = body ? JSON.parse(body) : null;

  if (!data || !isValidRequest(data)) {
    logger.error('invalid request', data);
    return callback(null, createResponse(errors.Invalid.message, errors.Invalid.statusCode));
  }

  if (!userId) {
    return callback(
      null,
      createResponse(errors.Unauthorized.message, errors.Unauthorized.statusCode),
    );
  }

  const tournamentId = generateRowId();

  return tournamentController
    .create(tournamentId, userId, data)
    .then(tournament => callback(null, createResponse(tournament)))
    .catch((error) => {
      const statusCode = error.statusCode || 400;
      logger.error(error);
      callback(null, createResponse(error.message, statusCode));
    });
};
