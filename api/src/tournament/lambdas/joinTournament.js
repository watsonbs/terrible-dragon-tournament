const tournamentController = require('../tournamentController');
const createResponse = require('../../utilities/createResponse');
const getUserId = require('../../utilities/getUserId');
const errors = require('../../errors');

const isValidRequest = gameId => Boolean(gameId);

exports.handler = (event, context, callback) => {
  const { pathParameters } = event;
  const userId = getUserId(event);
  const gameId = pathParameters.tournamentId;

  if (!isValidRequest(gameId)) {
    return callback(null, createResponse(errors.Invalid.message, errors.Invalid.statusCode));
  }

  if (!userId) {
    return callback(
      null,
      createResponse(errors.Unauthorized.message, errors.Unauthorized.statusCode),
    );
  }

  return tournamentController
    .join(userId, gameId)
    .then(() => callback(null, createResponse({ ok: true })))
    .catch((error) => {
      const statusCode = error.statusCode || 400;
      callback(null, createResponse(error.message, statusCode));
    });
};
