const mockRandomWith = require('jest-mock-random').mockRandom;
const createTournament = require('./createTournament');
const tournamentController = require('../tournamentController');
const errors = require('../../errors');
const mockEvent = require('../../../test-utilities/mocks/validRequestEvent');

jest.mock('../../utilities/logger', () => ({
  log: jest.fn(),
  error: jest.fn(),
}));

const context = {};
const realDateNow = Date.now;

describe('createTournament lambda', () => {
  beforeEach(() => {
    Date.now = () => 1522658966857;
    mockRandomWith(0.7);
    tournamentController.create = jest.fn();
    tournamentController.addGameToPlayer = jest.fn();
  });
  afterEach(() => {
    Date.now = realDateNow;
  });
  describe('successful requests', () => {
    test('creates a tournament and adds current player to it', (done) => {
      tournamentController.create.mockImplementation(() =>
        Promise.resolve({
          TournamentId: '7296089025970534',
          TournamentName: 'My Game',
          RoundCount: 1,
          CurrentRound: 0,
          TickLength: 60,
          ExpiresAt: 1538470166857,
          Administrator: 'userId',
          Players: ['userId'],
        }));

      createTournament.handler(mockEvent, context, (error, response) => {
        expect(response).toEqual({
          statusCode: 200,
          body: JSON.stringify({
            TournamentId: '7296089025970534',
            TournamentName: 'My Game',
            RoundCount: 1,
            CurrentRound: 0,
            TickLength: 60,
            ExpiresAt: 1538470166857,
            Administrator: 'userId',
            Players: ['userId'],
          }),
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        });
        expect(tournamentController.create).toHaveBeenCalledWith('7296089025970534', 'userId', {
          gameName: 'My Game',
          tickLength: 60,
          roundCount: 1,
        });

        done();
      });
    });
  });
  // TODO - unsuccessful requests
  describe('invalid requests', () => {
    const invalidRequestResponse = {
      statusCode: errors.Invalid.statusCode,
      body: errors.Invalid.message,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
    };
    test('rejects no POST params', done =>
      createTournament.handler(
        { requestContext: mockEvent.requestContext },
        context,
        (error, response) => {
          expect(response).toEqual(invalidRequestResponse);
          done();
        },
      ));
    test('rejects no gameName', done =>
      createTournament.handler(
        {
          requestContext: mockEvent.requestContext,
          body: JSON.stringify({ tickLength: 60, roundCount: 1 }),
        },
        context,
        (error, response) => {
          expect(response).toEqual(invalidRequestResponse);
          done();
        },
      ));
    test('rejects no roundCount', done =>
      createTournament.handler(
        {
          requestContext: mockEvent.requestContext,
          body: JSON.stringify({ gameName: 'My Game', tickLength: 60 }),
        },
        context,
        (error, response) => {
          expect(response).toEqual(invalidRequestResponse);
          done();
        },
      ));
    test('rejects no tickLength', done =>
      createTournament.handler(
        {
          requestContext: mockEvent.requestContext,
          body: JSON.stringify({ gameName: 'My Game', roundCount: 1 }),
        },
        context,
        (error, response) => {
          expect(response).toEqual(invalidRequestResponse);
          done();
        },
      ));
    test('rejects overlong gameName', done =>
      createTournament.handler(
        {
          body: JSON.stringify({
            gameName: '12345678901234567890123456789012345678901234567890123456789012345',
            tickLength: 60,
            roundCount: 1,
          }),
          requestContext: mockEvent.requestContext,
        },
        context,
        (error, response) => {
          expect(response).toEqual(invalidRequestResponse);
          done();
        },
      ));
    test('rejects too short tickLength', done =>
      createTournament.handler(
        {
          body: JSON.stringify({
            gameName: 'My game',
            tickLength: 29,
            roundCount: 1,
          }),
          requestContext: mockEvent.requestContext,
        },
        context,
        (error, response) => {
          expect(response).toEqual(invalidRequestResponse);
          done();
        },
      ));
  });
  describe('unauthorized requests', () => {
    const unauthorizedRequestResponse = {
      statusCode: errors.Unauthorized.statusCode,
      body: errors.Unauthorized.message,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
    };
    const tokenValidationError = errors.Unauthorized;
    tokenValidationError.statusCode = 401;
    test('rejects invalid tokens', (done) => {
      createTournament.handler(
        { body: JSON.stringify({ tickLength: 60, roundCount: 1, gameName: 'My Game' }) },
        context,
        (error, response) => {
          expect(response).toEqual(unauthorizedRequestResponse);
          done();
        },
      );
    });
  });
});
