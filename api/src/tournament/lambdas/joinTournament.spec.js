const joinTournament = require('./joinTournament');
const tournamentController = require('../tournamentController');
const errors = require('../../errors');
const mockEvent = require('../../../test-utilities/mocks/validRequestEvent');

tournamentController.join = jest.fn();
const context = {};
const MOCK_TOURNAMENT = {
  ResourceId: 'gameId',
  TournamentName: 'My Game',
  RoundCount: 1,
  CurrentRound: 0,
  TickLength: 60,
  ExpiresAt: 1538470166857,
  Administrator: 'admin',
  Players: ['admin', 'userId'],
};

describe('joinTournament Lambda', () => {
  describe('successful requests', () => {
    beforeEach(() => {
      tournamentController.join.mockImplementation(() => Promise.resolve(MOCK_TOURNAMENT));
    });
    test('joins tournament and returns it if it is permitted', (done) => {
      joinTournament.handler(
        {
          requestContext: mockEvent.requestContext,
          pathParameters: { tournamentId: 'gameId' },
        },
        context,
        (error, response) => {
          expect(response).toEqual({
            body: JSON.stringify({ ok: true }),
            statusCode: 200,
            headers: {
              'Access-Control-Allow-Origin': '*',
            },
          });
          expect(tournamentController.join).toHaveBeenCalledWith('userId', 'gameId');
          done();
        },
      );
    });
  });

  // TODO - unsuccessful requests
  describe('invalid requests', () => {
    const invalidRequestResponse = {
      statusCode: 400,
      body: 'Invalid Request',
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
    };

    test('rejects requests without a tournamentId', (done) => {
      joinTournament.handler(
        { requestContext: mockEvent.requestContext, pathParameters: {} },
        context,
        (error, response) => {
          expect(response).toEqual(invalidRequestResponse);
          done();
        },
      );
    });
  });
  describe('unauthorized requests', () => {
    test('rejects missing userId', (done) => {
      joinTournament.handler(
        { pathParameters: { tournamentId: 'gameId' } },
        context,
        (error, response) => {
          expect(response).toEqual({
            statusCode: errors.Unauthorized.statusCode,
            body: errors.Unauthorized.message,
            headers: {
              'Access-Control-Allow-Origin': '*',
            },
          });
          done();
        },
      );
    });
  });
});
