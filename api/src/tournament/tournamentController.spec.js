const AWS = require('aws-sdk-mock');
const tournamentController = require('./tournamentController');
const getInitialGameState = require('../games/getInitialGameState');

const TABLE_NAME = 'terrible-dragon_tournaments';

const mockInitialGameState = {
  InternalGameState: { isInternal: true },
  GameState: { isPublic: true },
  PlayerState: { playerA: { isPrivate: true } },
};
jest.mock('../games/getInitialGameState', () => jest.fn(() => mockInitialGameState));

const realDateNow = Date.now;

let dynamodbMocks;
describe('tournamentController', () => {
  beforeEach(() => {
    dynamodbMocks = {
      putItem: jest.fn(),
      updateItem: jest.fn(),
      batchGetItem: jest.fn(),
      getItem: jest.fn(),
    };

    AWS.mock('DynamoDB', 'putItem', (params, callback) => dynamodbMocks.putItem(params, callback));
    AWS.mock('DynamoDB', 'getItem', (params, callback) => dynamodbMocks.getItem(params, callback));
    AWS.mock('DynamoDB', 'updateItem', (params, callback) =>
      dynamodbMocks.updateItem(params, callback));
    AWS.mock('DynamoDB', 'batchGetItem', (params, callback) =>
      dynamodbMocks.batchGetItem(params, callback));
  });

  describe('create', () => {
    const createdTournament = {
      TournamentId: 'tournamentId',
      TournamentName: 'My Game',
      RoundCount: 1,
      CurrentRound: 0,
      TickLength: 60,
      ExpiresAt: 1538470166857,
      Administrator: 'creatorId',
      Players: ['creatorId'],
    };
    const tournamentConfig = { gameName: 'My Game', tickLength: 60, roundCount: 1 };
    beforeEach(() => {
      Date.now = () => 1522658966857;
      dynamodbMocks.putItem.mockImplementationOnce((params, callback) =>
        callback(null, 'successfully put item in database'));
      dynamodbMocks.updateItem.mockImplementation((params, callback) =>
        callback(null, 'successfully updated item in database'));
    });

    afterEach(() => {
      Date.now = realDateNow;
    });

    test('puts a new tournament into Dynamo', (done) => {
      tournamentController
        .create('tournamentId', 'creatorId', tournamentConfig)
        .then((tournament) => {
          expect(tournament).toEqual(createdTournament);
          expect(dynamodbMocks.putItem).toHaveBeenCalledWith(
            {
              Item: {
                Administrator: { S: 'creatorId' },
                CurrentRound: { N: '0' },
                ExpiresAt: { N: '1538470166857' },
                Players: { SS: ['creatorId'] },
                ResourceId: { S: 'tournamentId' },
                RoundCount: { N: '1' },
                TickLength: { N: '60' },
                TournamentName: { S: 'My Game' },
              },
              TableName: 'terrible-dragon_tournaments',
            },
            expect.any(Function),
          );
          done();
        });
    });

    test("updates the player's active games list if it exists", (done) => {
      tournamentController
        .create('tournamentId', 'creatorId', tournamentConfig)
        .then((tournament) => {
          expect(tournament).toEqual(createdTournament);
          expect(dynamodbMocks.updateItem).toHaveBeenCalledWith(
            {
              TableName: TABLE_NAME,
              Key: {
                ResourceId: { S: 'creatorId' },
              },
              ExpressionAttributeValues: {
                ':tid': { SS: ['tournamentId'] },
              },
              UpdateExpression: 'ADD ActiveGames :tid',
            },
            expect.any(Function),
          );
          done();
        });
    });

    test("creates a player's active games list if it does not exist", (done) => {
      dynamodbMocks.putItem.mockImplementation((params, callback) =>
        callback(null, 'successfully put item in database'));
      dynamodbMocks.updateItem.mockImplementationOnce((params, callback) =>
        callback({ StatusCode: 'ResourceNotFoundException' }));

      tournamentController.create('tournamentId', 'creatorId', tournamentConfig).then(() => {
        expect(dynamodbMocks.putItem).toHaveBeenCalledWith(
          {
            TableName: TABLE_NAME,
            Item: {
              ResourceId: { S: 'creatorId' },
              ActiveGames: { SS: ['tournamentId'] },
              ArchivedGames: { SS: [] },
            },
          },
          expect.any(Function),
        );
        done();
      });
    });
  });

  describe('join', () => {
    test("adds player to the game's player list", () => {
      dynamodbMocks.updateItem.mockImplementation((params, callback) =>
        callback(null, 'successfully updated item in database'));

      tournamentController.join('joinerId', 'tournamentId').then(() => {
        expect(dynamodbMocks.updateItem).toHaveBeenCalledWith(
          {
            TableName: TABLE_NAME,
            Key: {
              ResourceId: { S: 'tournamentId' },
            },
            ExpressionAttributeValues: {
              ':uid': { SS: ['joinerId'] },
            },
            UpdateExpression: 'ADD Players :uid',
            ConditionExpression: 'attribute_exists(ResourceId)',
          },
          expect.any(Function),
        );
      });
    });

    test("updates the player's active games list if it exists", (done) => {
      dynamodbMocks.updateItem.mockImplementation((params, callback) =>
        callback(null, 'successfully updated item in database'));

      tournamentController.join('joinerId', 'tournamentId').then(() => {
        expect(dynamodbMocks.updateItem).toHaveBeenCalledWith(
          {
            TableName: TABLE_NAME,
            Key: {
              ResourceId: { S: 'joinerId' },
            },
            ExpressionAttributeValues: {
              ':tid': { SS: ['tournamentId'] },
            },
            UpdateExpression: 'ADD ActiveGames :tid',
          },
          expect.any(Function),
        );
        done();
      });
    });

    test("creates a player's active games list if it does not exist", (done) => {
      dynamodbMocks.updateItem
        .mockImplementationOnce((params, callback) =>
          callback(null, 'successfully put item in database'))
        .mockImplementationOnce((params, callback) =>
          callback({ StatusCode: 'ResourceNotFoundException' }));
      dynamodbMocks.putItem.mockImplementation((params, callback) =>
        callback(null, 'successfully put item in database'));

      tournamentController.join('joinerId', 'tournamentId').then(() => {
        expect(dynamodbMocks.putItem).toHaveBeenCalledWith(
          {
            TableName: TABLE_NAME,
            Item: {
              ResourceId: { S: 'joinerId' },
              ActiveGames: { SS: ['tournamentId'] },
              ArchivedGames: { SS: [] },
            },
          },
          expect.any(Function),
        );
        done();
      });
    });
  });

  describe('getTournaments', () => {
    test('returns a list of tournaments from a given list of tournamentIds', (done) => {
      dynamodbMocks.batchGetItem.mockImplementation((params, callback) =>
        callback(null, {
          Responses: {
            [TABLE_NAME]: [
              {
                Administrator: { S: 'creatorId' },
                CurrentRound: { N: '0' },
                ExpiresAt: { N: '1538470166857' },
                Players: { SS: ['creatorId'] },
                ResourceId: { S: 'tournamentId' },
                RoundCount: { N: '1' },
                TickLength: { N: '60' },
                TournamentName: { S: 'My Game' },
              },
              {
                Administrator: { S: 'creatorId' },
                CurrentRound: { N: '0' },
                ExpiresAt: { N: '1538470166857' },
                Players: { SS: ['creatorId'] },
                ResourceId: { S: 'tournamentId2' },
                RoundCount: { N: '1' },
                TickLength: { N: '60' },
                TournamentName: { S: 'My Game' },
              },
            ],
          },
        }));

      tournamentController.getTournaments(['tournamentId', 'tournamentId2']).then((tournaments) => {
        expect(dynamodbMocks.batchGetItem).toHaveBeenCalledWith(
          {
            RequestItems: {
              [TABLE_NAME]: {
                Keys: [
                  {
                    ResourceId: { S: 'tournamentId' },
                  },
                  {
                    ResourceId: { S: 'tournamentId2' },
                  },
                ],
              },
            },
          },
          expect.any(Function),
        );

        expect(tournaments).toEqual([
          {
            Administrator: 'creatorId',
            CurrentRound: 0,
            ExpiresAt: 1538470166857,
            Players: ['creatorId'],
            ResourceId: 'tournamentId',
            RoundCount: 1,
            TickLength: 60,
            TournamentName: 'My Game',
          },
          {
            Administrator: 'creatorId',
            CurrentRound: 0,
            ExpiresAt: 1538470166857,
            Players: ['creatorId'],
            ResourceId: 'tournamentId2',
            RoundCount: 1,
            TickLength: 60,
            TournamentName: 'My Game',
          },
        ]);

        done();
      });
    });
  });

  describe('start', () => {
    test('it selects a random gameType and initialises the GameState', () => {
      dynamodbMocks.getItem.mockImplementation((params, callback) =>
        callback(null, {
          Item: {
            Players: { SS: ['playerA', 'playerB', 'playerC'] },
            Administrator: { S: 'playerA' },
            TickLength: { N: 600 },
          },
        }));

      dynamodbMocks.updateItem.mockImplementation((params, callback) => {
        callback(null, {
          Item: {},
        });
      });

      const mockTournamentId = '123456';
      const expectedInitialState = {
        ExpressionAttributeValues: {
          ':gs': { M: { isPublic: { BOOL: true } } },
          ':igs': { M: { isInternal: { BOOL: true } } },
          ':ps': { M: { playerA: { M: { isPrivate: { BOOL: true } } } } },
        },
        Key: { ResourceId: { S: '123456' } },
        TableName: 'terrible-dragon_tournaments',
        UpdateExpression: 'SET GameState = :gs, InternalGameState = :igs, PlayerState = :ps',
      };

      return tournamentController.start('playerA', mockTournamentId).then((tournamentState) => {
        expect(getInitialGameState).toHaveBeenCalledWith(
          'commonsPot',
          ['playerA', 'playerB', 'playerC'],
          { tickLength: 600 },
        );
        expect(dynamodbMocks.updateItem).toHaveBeenCalledWith(
          expectedInitialState,
          expect.any(Function),
        );
        expect(tournamentState).toEqual({
          GameState: { isPublic: true },
          PlayerState: { isPrivate: true },
        });
      });
    });

    test('it will not start a game if the authorising user is not the game administrator', () => {
      dynamodbMocks.getItem.mockImplementation((params, callback) =>
        callback(null, {
          Item: {
            Players: { SS: ['playerA', 'playerB', 'playerC'] },
            Administrator: { S: 'playerA' },
          },
        }));

      const mockTournamentId = '123456';

      return tournamentController
        .start('playerB', mockTournamentId)
        .then(() => expect(true).toBeFalsy())
        .catch(() => {
          expect(dynamodbMocks.updateItem).not.toHaveBeenCalled();
        });
    });

    test('it will not start a game if there are not enough players', () => {
      dynamodbMocks.getItem.mockImplementation((params, callback) =>
        callback(null, {
          Item: {
            Players: { SS: ['playerA', 'playerB'] },
            Administrator: { S: 'playerA' },
          },
        }));

      const mockTournamentId = '123456';

      return tournamentController
        .start('playerA', mockTournamentId)
        .then(() => expect(true).toBeFalsy())
        .catch((err) => {
          expect(err.message).toBe('You need at least 3 players to start a tournament');
          expect(dynamodbMocks.updateItem).not.toHaveBeenCalled();
        });
    });
  });
});
