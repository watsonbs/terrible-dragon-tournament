const config = require('../../config');
const dynamodb = require('../../utilities/dynamodb');
const addGameToPlayer = require('./_addGameToPlayer');

const addPlayerToGame = (tournamentId, userId) =>
  new Promise((resolve, reject) => {
    const params = {
      TableName: config.TournamentTableName,
      Key: {
        ResourceId: { S: tournamentId },
      },
      ExpressionAttributeValues: {
        ':uid': { SS: [userId] },
      },
      UpdateExpression: 'ADD Players :uid',
      ConditionExpression: 'attribute_exists(ResourceId)',
    };
    dynamodb.instance().updateItem(params, (err, data) => {
      if (err) reject(err);
      if (!data) reject(new Error('No response data'));
      resolve(data);
    });
  });

const join = (userId, tournamentId) =>
  addPlayerToGame(tournamentId, userId).then(() => addGameToPlayer(userId, tournamentId));

module.exports = join;
