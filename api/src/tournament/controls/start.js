const config = require('../../config');
const dynamodb = require('../../utilities/dynamodb');
const dynamoConverter = require('../../utilities/dynamoConverter');
const randomBetween = require('../../utilities/randomBetween');
const gameList = require('../../games/game-list.json');
const getInitialGameState = require('../../games/getInitialGameState');
const errors = require('../../errors');

module.exports = (authorisingUser, tournamentId) =>
  dynamodb
    .getItem({
      TableName: config.TournamentTableName,
      Key: {
        ResourceId: { S: tournamentId },
      },
      ProjectionExpression: 'Players,Administrator,TickLength',
    })
    .then((tournamentDynamoData) => {
      const tournamentData = dynamoConverter.convertFromDynamo(tournamentDynamoData.Item);
      if (tournamentData.Administrator !== authorisingUser) {
        throw errors.Forbidden;
      }

      const players = tournamentData.Players;

      if (players.length < config.MinimumPlayers) {
        throw new Error(`You need at least ${config.MinimumPlayers} players to start a tournament`);
      }

      const tournamentConfig = { tickLength: tournamentData.TickLength };
      const gameType = gameList[randomBetween(1, gameList.length) - 1];
      const initialState = getInitialGameState(gameType, players, tournamentConfig);

      return dynamodb
        .updateItem({
          TableName: config.TournamentTableName,
          Key: {
            ResourceId: { S: tournamentId },
          },
          ExpressionAttributeValues: {
            ':gs': { M: dynamoConverter.convertToDynamo(initialState.GameState) },
            ':igs': { M: dynamoConverter.convertToDynamo(initialState.InternalGameState) },
            ':ps': { M: dynamoConverter.convertToDynamo(initialState.PlayerState) },
          },
          UpdateExpression: 'SET GameState = :gs, InternalGameState = :igs, PlayerState = :ps',
        })
        .then(() => ({
          GameState: initialState.GameState,
          PlayerState: initialState.PlayerState[authorisingUser],
        }));
    });
