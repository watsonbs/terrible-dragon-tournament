const config = require('../../config');
const dynamodb = require('../../utilities/dynamodb');
const dynamoConverter = require('../../utilities/dynamoConverter');

const getTournaments = tournamentIds =>
  new Promise((resolve, reject) => {
    const params = {
      RequestItems: {
        [config.TournamentTableName]: {
          Keys: tournamentIds.map(tournamentId => ({
            ResourceId: {
              S: tournamentId,
            },
          })),
        },
      },
    };
    dynamodb.instance().batchGetItem(params, (err, data) => {
      if (err) return reject(err);
      if (!data || !data.Responses || !data.Responses[config.TournamentTableName]) {
        return reject(new Error('No Response'));
      }
      const tournaments = data.Responses[config.TournamentTableName].map(dynamoItem =>
        dynamoConverter.convertFromDynamo(dynamoItem));
      return resolve(tournaments);
    });
  });

module.exports = getTournaments;
