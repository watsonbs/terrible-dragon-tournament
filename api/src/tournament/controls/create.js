const config = require('../../config');
const dynamodb = require('../../utilities/dynamodb');
const dynamoConverter = require('../../utilities/dynamoConverter');
const addGameToPlayer = require('./_addGameToPlayer');

const create = (tournamentId, creatorId, { gameName, tickLength, roundCount }) =>
  new Promise((resolve, reject) => {
    const tournament = {
      ResourceId: tournamentId,
      TournamentName: gameName,
      RoundCount: roundCount,
      CurrentRound: 0,
      TickLength: tickLength,
      ExpiresAt: Date.now() + config.TournamentTTL,
      Administrator: creatorId,
      Players: [creatorId],
    };
    const params = {
      TableName: config.TournamentTableName,
      Item: dynamoConverter.convertToDynamo(tournament),
    };
    dynamodb.instance().putItem(params, (err, data) => {
      if (err) return reject(err);
      if (!data) return reject(new Error('no response'));
      return addGameToPlayer(creatorId, tournamentId).then(() => {
        tournament.TournamentId = tournament.ResourceId;
        delete tournament.ResourceId;
        resolve(tournament);
      });
      // TODO - rollback if only one fails
    });
  });

module.exports = create;
