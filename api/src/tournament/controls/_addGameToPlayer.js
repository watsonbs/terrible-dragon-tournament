const config = require('../../config');
const dynamodb = require('../../utilities/dynamodb');

const addGameToPlayer = (userId, tournamentId) =>
  new Promise((resolve, reject) => {
    const params = {
      TableName: config.TournamentTableName,
      Key: {
        ResourceId: { S: userId },
      },
      ExpressionAttributeValues: {
        ':tid': { SS: [tournamentId] },
      },
      UpdateExpression: 'ADD ActiveGames :tid',
    };

    dynamodb.instance().updateItem(params, (updateError, updateData) => {
      if (updateError) {
        if (updateError.StatusCode === 'ResourceNotFoundException') {
          dynamodb.instance().putItem(
            {
              TableName: config.TournamentTableName,
              Item: {
                ResourceId: { S: userId },
                ActiveGames: { SS: [tournamentId] },
                ArchivedGames: { SS: [] },
              },
            },
            (putError, putData) => {
              if (putError) reject(putError);
              resolve(putData);
            },
          );
        } else {
          reject(updateError);
        }
      } else {
        resolve(updateData);
      }
    });
  });

module.exports = addGameToPlayer;
