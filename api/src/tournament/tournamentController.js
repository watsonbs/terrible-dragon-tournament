const start = require('./controls/start');
const getTournaments = require('./controls/getTournaments');
const join = require('./controls/join');
const create = require('./controls/create');

module.exports = {
  join,
  create,
  getTournaments,
  start,
};
