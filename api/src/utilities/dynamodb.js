const AWS = require('aws-sdk'); // eslint-disable-line import/no-extraneous-dependencies

let dynamodb;
const instance = () => {
  if (!dynamodb) {
    dynamodb = new AWS.DynamoDB({ apiVersion: '2012-08-10', region: 'eu-west-1' });
  }

  return dynamodb;
};

const updateItem = params =>
  new Promise((resolve, reject) => {
    instance().updateItem(params, (err, data) => {
      if (err) return reject(err);
      if (!data) return reject(new Error('No response data'));
      return resolve(data);
    });
  });

const getItem = params =>
  new Promise((resolve, reject) => {
    instance().getItem(params, (err, data) => {
      if (err) return reject(err);
      if (!data) return reject(new Error('No response data'));
      return resolve(data);
    });
  });

module.exports = {
  instance,
  updateItem,
  getItem,
};
