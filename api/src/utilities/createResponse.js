module.exports = (body, statusCode) => ({
  statusCode: statusCode || 200,
  body: typeof body === 'object' ? JSON.stringify(body) : body,
  headers: {
    'Access-Control-Allow-Origin': '*',
  },
});
