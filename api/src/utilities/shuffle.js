module.exports = (inputArray) => {
  const shuffledArray = [...inputArray];
  let counter = shuffledArray.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    const index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--; // eslint-disable-line no-plusplus

    // And swap the last element with it
    const temp = shuffledArray[counter];
    shuffledArray[counter] = shuffledArray[index];
    shuffledArray[index] = temp;
  }

  return shuffledArray;
};
