/* Copyright 2017-2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file
 except in compliance with the License. A copy of the License is located at

     http://aws.amazon.com/apache2.0/

 or in the "license" file accompanying this file. This file is distributed on an "AS IS"
 BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 License for the specific language governing permissions and limitations under the License.
*/

const https = require('https');
const jose = require('node-jose');
const errors = require('../errors');

const region = 'eu-west-1';
const userpoolId = 'eu-west-1_AYmk2IClx';
const appClientId = '1vldh2892va7c0uafugq4je3go';
const keysUrl = `https://cognito-idp.${region}.amazonaws.com/${userpoolId}/.well-known/jwks.json`;

const validateToken = (token, callback) => {
  if (!token) callback('no token supplied');
  const sections = token.split('.');
  // get the kid from the headers prior to verification
  let header = jose.util.base64url.decode(sections[0]);
  header = JSON.parse(header);
  const { kid } = header;
  // download the public keys
  https.get(keysUrl, (response) => {
    if (response.statusCode === 200) {
      response.on('data', (body) => {
        const { keys } = JSON.parse(body);
        // search for the kid in the downloaded public keys
        let keyIndex = -1;
        for (let i = 0; i < keys.length; i + 1) {
          if (kid === keys[i].kid) {
            keyIndex = i;
            break;
          }
        }
        if (keyIndex === -1) {
          callback('Public key not found in jwks.json');
        }
        // construct the public key
        jose.JWK.asKey(keys[keyIndex]).then((publicKey) => {
          // verify the signature
          jose.JWS.createVerify(publicKey)
            .verify(token)
            .then((result) => {
              // now we can use the claims
              const claims = JSON.parse(result.payload);
              // additionally we can verify the token expiration
              const currentTs = Math.floor(new Date() / 1000);
              if (currentTs > claims.exp) {
                callback('Token is expired');
              }
              // and the Audience (use claims.client_id if verifying an access token)
              if (claims.aud !== appClientId) {
                callback('Token was not issued for this audience');
              }
              callback(null, claims);
            })
            .catch(() => {
              callback('Signature verification failed');
            });
        });
      });
    }
  });
};

exports.validate = token =>
  new Promise((resolve, reject) => {
    if (!token) reject(errors.Unauthorized);

    validateToken(token, (error, response) => {
      if (error) {
        return reject(errors.Unauthorized);
      }
      return resolve(response);
    });
  });
