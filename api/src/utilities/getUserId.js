module.exports = ({ requestContext }) => {
  const username =
    requestContext &&
    requestContext.authorizer &&
    requestContext.authorizer.claims &&
    requestContext.authorizer.claims['cognito:username'];
  return username;
};
