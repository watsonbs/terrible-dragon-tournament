const convertToDynamoProperty = {
  string: string => ({ S: string }),
  number: number => ({ N: number.toString() }),
  boolean: boolean => ({ BOOL: boolean }),
  object: (object) => {
    if (Array.isArray(object)) {
      return { SS: object };
    }
    return {
      M: convertToDynamo(object), // eslint-disable-line no-use-before-define
    };
  },
};

const convertToDynamo = (object) => {
  const keys = Object.keys(object);
  return keys.reduce((dynamoItem, key) => {
    const value = object[key];
    return Object.assign({ [key]: convertToDynamoProperty[typeof value](value) }, dynamoItem);
  }, {});
};

const convertFromDynamoProperty = {
  S: string => string,
  SS: array => array,
  N: number => parseFloat(number),
  M: map => convertFromDynamo(map), // eslint-disable-line no-use-before-define
  BOOL: boolean => boolean,
};
const convertFromDynamo = (dynamoItem) => {
  const properties = Object.keys(dynamoItem);
  return properties.reduce((object, propertyName) => {
    const property = dynamoItem[propertyName];
    const propertyType = Object.keys(property)[0];
    const value = property[propertyType];
    return Object.assign({}, object, {
      [propertyName]: convertFromDynamoProperty[propertyType](value),
    });
  }, {});
};

module.exports = {
  convertToDynamo,
  convertFromDynamo,
};
