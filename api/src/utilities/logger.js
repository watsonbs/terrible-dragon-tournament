module.exports = {
  error: (...args) => {
    console.error(...args); // eslint-disable-line no-console
  },
  log: (...args) => {
    console.log(...args); // eslint-disable-line no-console
  },
};
