const sanitizeTournament = (userId, tournament) => {
  if (!tournament) return tournament;
  const sanitizedTournament = Object.assign({}, tournament, {
    PlayerState: tournament.PlayerState ? tournament.PlayerState[userId] : null,
    TournamentId: tournament.ResourceId,
  });
  delete sanitizedTournament.ResourceId;

  if (sanitizedTournament.InternalGameState) {
    delete sanitizedTournament.InternalGameState;
  }
  return sanitizedTournament;
};

module.exports = sanitizeTournament;
