const touramentController = require('../../tournament/tournamentController');
const sanitizeTournament = require('./_sanitizeTournament');
const errors = require('../../errors');

const getTournament = (userId, tournamentId) =>
  touramentController.getTournaments([tournamentId]).then((tournaments) => {
    if (tournaments.length < 1) throw errors.NotFound;
    const tournament = tournaments[0];
    if (tournament.Players.indexOf(userId) === -1) throw errors.Unauthorized;
    return sanitizeTournament(userId, tournament);
  });

module.exports = getTournament;
