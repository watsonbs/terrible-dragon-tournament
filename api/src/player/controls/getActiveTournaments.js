const config = require('../../config');
const touramentController = require('../../tournament/tournamentController');
const dynamoConverter = require('../../utilities/dynamoConverter');
const dynamodb = require('../../utilities/dynamodb');

const summarizeTournament = (
  userId,
  {
    ResourceId, NextTick, PlayerState, TournamentName, CurrentRound, Administrator,
  },
) => ({
  TournamentId: ResourceId,
  TournamentName,
  Submitted: PlayerState && PlayerState[userId] ? Boolean(PlayerState[userId].submitted) : false,
  NextTick,
  CurrentRound,
  isAdmin: Administrator === userId,
});
const getActiveTournaments = userId =>
  new Promise((resolve, reject) => {
    const params = {
      Key: {
        ResourceId: { S: userId },
      },
      TableName: config.TournamentTableName,
    };
    dynamodb.instance().getItem(params, (err, playerResponse) => {
      if (err) return reject(err);
      if (!playerResponse || !playerResponse.Item) {
        return resolve([]);
      }

      const playerData = dynamoConverter.convertFromDynamo(playerResponse.Item);

      if (!playerData.ActiveGames || !playerData.ActiveGames.length) {
        return resolve([]);
      }

      return touramentController
        .getTournaments(playerData.ActiveGames)
        .then(tournaments => tournaments.map(tournament => summarizeTournament(userId, tournament)))
        .then(sanitizedTournaments => resolve(sanitizedTournaments));
    });
  });

module.exports = getActiveTournaments;
