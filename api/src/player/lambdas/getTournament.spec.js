const getTournament = require('./getTournament');
const mockTournament = require('../../../test-utilities/mocks/tournament');
const errors = require('../../errors');
const mockEvent = require('../../../test-utilities/mocks/validRequestEvent');

const mockPlayerController = {
  getTournament: jest.fn(() => Promise.resolve(mockTournament)),
};

jest.mock('../createPlayerController', () => () => mockPlayerController);

const MOCK_TOURNAMENT_ID = 'tournamentId';
const context = {};

describe('getTournament spec', () => {
  describe('successful attempts', () => {
    test('requests the tournaments attached to the player', (done) => {
      getTournament.handler(
        {
          requestContext: mockEvent.requestContext,
          pathParameters: { tournamentId: MOCK_TOURNAMENT_ID },
        },
        context,
        (error, response) => {
          expect(mockPlayerController.getTournament).toHaveBeenCalledWith(MOCK_TOURNAMENT_ID);
          expect(response).toEqual({
            statusCode: 200,
            body: JSON.stringify(mockTournament),
            headers: {
              'Access-Control-Allow-Origin': '*',
            },
          });
          done();
        },
      );
    });
  });

  describe('unauthorized requests', () => {
    test('rejects requests without a user', (done) => {
      const unauthorizedRequestResponse = {
        statusCode: errors.Unauthorized.statusCode,
        body: errors.Unauthorized.message,
        headers: {
          'Access-Control-Allow-Origin': '*',
        },
      };
      getTournament.handler(
        { pathParameters: { tournamentId: MOCK_TOURNAMENT_ID } },
        context,
        (error, response) => {
          expect(response).toEqual(unauthorizedRequestResponse);
          done();
        },
      );
    });
  });
});
