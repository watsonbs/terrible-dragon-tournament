const takeGameAction = require('./takeGameAction');
const gameController = require('../../games/gameController');
const mockEvent = require('../../../test-utilities/mocks/validRequestEvent');

jest.mock('../../games/gameController', () => ({
  mockGame: {
    mockAction: jest.fn(),
  },
}));

const MOCK_TOURNAMENT_ID = '1234';

describe('takeGameAction Lambda', () => {
  const context = {};

  test('applies an action on a game', (done) => {
    const mockTournament = {
      GameState: { isPublic: true },
      InternalGameState: { isSecret: true },
      PlayerState: { userId: { isPrivate: true } },
    };

    gameController.mockGame.mockAction.mockReturnValueOnce(Promise.resolve(mockTournament));

    takeGameAction.handler(
      {
        requestContext: mockEvent.requestContext,
        pathParameters: {
          tournamentId: MOCK_TOURNAMENT_ID,
          gameType: 'mockGame',
          gameAction: 'mockAction',
        },
        data: {
          myParam: true,
        },
      },
      context,
      (error, response) => {
        expect(gameController.mockGame.mockAction).toHaveBeenCalledWith(
          'userId',
          MOCK_TOURNAMENT_ID,
          { myParam: true },
        );
        expect(response).toEqual({
          statusCode: 200,
          body: JSON.stringify(mockTournament),
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        });

        done();
      },
    );
  });

  test('returns an error if the game does not exist', (done) => {
    takeGameAction.handler(
      {
        requestContext: mockEvent.requestContext,
        pathParameters: {
          tournamentId: MOCK_TOURNAMENT_ID,
          gameType: 'notAGame',
          gameAction: 'mockAction',
        },
        data: {
          myParam: true,
        },
      },
      context,
      (error, response) => {
        expect(response).toEqual({
          statusCode: 400,
          body: 'Invalid game type',
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        });

        done();
      },
    );
  });

  test('returns an error if the game action does not exist', (done) => {
    takeGameAction.handler(
      {
        requestContext: mockEvent.requestContext,
        pathParameters: {
          tournamentId: MOCK_TOURNAMENT_ID,
          gameType: 'mockGame',
          gameAction: 'notAnAction',
        },
        data: {
          myParam: true,
        },
      },
      context,
      (error, response) => {
        expect(response).toEqual({
          statusCode: 400,
          body: 'Invalid game action',
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        });

        done();
      },
    );
  });

  test('returns an error if applying the action returns an error', (done) => {
    gameController.mockGame.mockAction.mockReturnValueOnce(Promise.reject(new Error('biffed')));

    takeGameAction.handler(
      {
        requestContext: mockEvent.requestContext,
        pathParameters: {
          tournamentId: MOCK_TOURNAMENT_ID,
          gameType: 'mockGame',
          gameAction: 'mockAction',
        },
        data: {
          myParam: true,
        },
      },
      context,
      (error, response) => {
        expect(response).toEqual({
          statusCode: 400,
          body: 'biffed',
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        });

        done();
      },
    );
  });
});

// /me/tournaments/tournamentId/action/mockGame/mockAction
