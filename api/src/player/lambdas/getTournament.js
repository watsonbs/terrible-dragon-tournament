const createResponse = require('../../utilities/createResponse');
const createPlayerController = require('../createPlayerController');
const getUserId = require('../../utilities/getUserId');
const errors = require('../../errors');

exports.handler = (event, context, callback) => {
  const { pathParameters } = event;
  const userId = getUserId(event);
  const { tournamentId } = pathParameters;

  if (!tournamentId) {
    return callback(null, createResponse('Invalid request', 400));
  }
  if (!userId) {
    return callback(
      null,
      createResponse(errors.Unauthorized.message, errors.Unauthorized.statusCode),
    );
  }
  const playerController = createPlayerController(userId);
  return playerController
    .getTournament(tournamentId)
    .then(tournament => callback(null, createResponse(tournament)))
    .catch((error) => {
      const statusCode = error.statusCode || 400;
      callback(null, createResponse(error.message, statusCode));
    });
};
