const createResponse = require('../../utilities/createResponse');
const createPlayerController = require('../createPlayerController');
const getUserId = require('../../utilities/getUserId');
const errors = require('../../errors');

exports.handler = (event, context, callback) => {
  const userId = getUserId(event);

  if (!userId) {
    return callback(
      null,
      createResponse(errors.Unauthorized.message, errors.Unauthorized.statusCode),
    );
  }

  const playerController = createPlayerController(userId);

  return playerController
    .getActiveTournaments()
    .then(tournaments => callback(null, createResponse(tournaments)))
    .catch((error) => {
      const statusCode = error.statusCode || 400;
      callback(null, createResponse(error.message, statusCode));
    });
};
