const getUserId = require('../../utilities/getUserId');
const gameController = require('../../games/gameController');
const createResponse = require('../../utilities/createResponse');
const errors = require('../../errors');

exports.handler = (event, context, callback) => {
  const { pathParameters, data } = event;
  const userId = getUserId(event);
  const { tournamentId, gameType, gameAction } = pathParameters;

  if (!tournamentId) {
    return callback(null, createResponse('Invalid request', 400));
  }
  if (!userId) {
    return callback(
      null,
      createResponse(errors.Unauthorized.message, errors.Unauthorized.statusCode),
    );
  }

  const game = gameController[gameType];

  if (!game) {
    return callback(null, createResponse('Invalid game type', errors.Invalid.statusCode));
  }

  const action = game[gameAction];

  if (!action) {
    return callback(null, createResponse('Invalid game action', errors.Invalid.statusCode));
  }

  return action(userId, tournamentId, data)
    .then(result => callback(null, createResponse(result)))
    .catch((error) => {
      const statusCode = error.statusCode || 400;
      callback(null, createResponse(error.message, statusCode));
    });
};
