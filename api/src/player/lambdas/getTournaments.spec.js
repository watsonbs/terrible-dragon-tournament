const getTournaments = require('./getTournaments');
const errors = require('../../errors');
const mockEvent = require('../../../test-utilities/mocks/validRequestEvent');

const context = {};
const MOCK_TOURNAMENTS = ['my-mock', 'my-other-mock'];
const mockPlayerController = {
  getActiveTournaments: jest.fn(() => Promise.resolve(MOCK_TOURNAMENTS)),
};

jest.mock('../createPlayerController', () => () => mockPlayerController);

describe('getTournaments spec', () => {
  describe('successful attempts', () => {
    test('requests the tournaments attached to the player', (done) => {
      getTournaments.handler(
        { requestContext: mockEvent.requestContext },
        context,
        (error, response) => {
          expect(mockPlayerController.getActiveTournaments).toHaveBeenCalled();
          expect(response).toEqual({
            statusCode: 200,
            body: JSON.stringify(MOCK_TOURNAMENTS),
            headers: {
              'Access-Control-Allow-Origin': '*',
            },
          });
          done();
        },
      );
    });
  });

  describe('unauthorized requests', () => {
    test('rejects requests with an invalid token', (done) => {
      const unauthorizedRequestResponse = {
        statusCode: errors.Unauthorized.statusCode,
        body: errors.Unauthorized.message,
        headers: {
          'Access-Control-Allow-Origin': '*',
        },
      };
      getTournaments.handler({}, context, (error, response) => {
        expect(response).toEqual(unauthorizedRequestResponse);
        done();
      });
    });
  });
});
