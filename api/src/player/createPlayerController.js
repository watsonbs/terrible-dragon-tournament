const getTournament = require('./controls/getTournament');
const getActiveTournaments = require('./controls/getActiveTournaments');

const init = (playerId) => {
  const userId = playerId;

  return {
    getTournament: tournamentId => getTournament(userId, tournamentId),
    getActiveTournaments: () => getActiveTournaments(userId),
  };
};

module.exports = init;
