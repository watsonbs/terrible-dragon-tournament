const AWS = require('aws-sdk-mock');
const createPlayerController = require('./createPlayerController');
const tournamentController = require('../tournament/tournamentController');

const TABLE_NAME = 'terrible-dragon_tournaments';
const userId = 'myUserId';
let dynamodbMocks;
let playerController;

describe('playerController', () => {
  const mockSummarizedTournamentList = [
    {
      TournamentId: 'tournamentId',
      TournamentName: 'My Game',
      NextTick: 1538470066857,
      Submitted: true,
      CurrentRound: 0,
      isAdmin: false,
    },
    {
      TournamentId: 'tournament2',
      TournamentName: 'My Other Game',
      NextTick: 1538470066857,
      Submitted: false,
      CurrentRound: 0,
      isAdmin: false,
    },
    {
      TournamentId: 'tournament3',
      TournamentName: 'My Third Game',
      NextTick: 1538470066857,
      Submitted: false,
      CurrentRound: 0,
      isAdmin: false,
    },
  ];
  beforeEach(() => {
    tournamentController.getTournaments = jest.fn();
    dynamodbMocks = {
      getItem: jest.fn(),
    };

    AWS.mock('DynamoDB', 'getItem', (params, callback) => dynamodbMocks.getItem(params, callback));

    playerController = createPlayerController(userId);
  });
  test("can return sumamries of all of the player's active tournaments for the active player", (done) => {
    tournamentController.getTournaments.mockImplementation(() =>
      Promise.resolve([
        {
          Administrator: 'admin',
          CurrentRound: 0,
          ExpiresAt: 1538470166857,
          Players: ['admin', 'myUserId'],
          ResourceId: 'tournamentId',
          RoundCount: 1,
          TickLength: 60,
          NextTick: 1538470066857,
          TournamentName: 'My Game',
          InternalGameState: {
            isPrivate: true,
          },
          GameState: {
            isPublic: true,
          },
          PlayerState: {
            myUserId: {
              isMyPlayerData: true,
              submitted: true,
            },
            admin: {
              isMyPlayerData: false,
            },
          },
        },
        {
          Administrator: 'admin',
          CurrentRound: 0,
          ExpiresAt: 1538470166857,
          Players: ['admin', 'myUserId', 'anotherPlayerId'],
          ResourceId: 'tournament2',
          RoundCount: 1,
          TickLength: 60,
          NextTick: 1538470066857,
          TournamentName: 'My Other Game',
          GameState: {
            isPublic: true,
          },
          PlayerState: {
            admin: {
              isMyPlayerData: false,
            },
            myUserId: {
              isMyPlayerData: true,
              submitted: false,
            },
            anotherPlayerId: {
              isMyPlayerData: false,
            },
          },
        },
        {
          Administrator: 'admin',
          CurrentRound: 0,
          ExpiresAt: 1538470166857,
          Players: ['admin', 'myUserId', 'anotherPlayerId'],
          ResourceId: 'tournament3',
          RoundCount: 1,
          TickLength: 60,
          NextTick: 1538470066857,
          TournamentName: 'My Third Game',
          GameState: {
            isPublic: true,
          },
          PlayerState: {
            admin: {
              isMyPlayerData: false,
            },
            myUserId: {
              isMyPlayerData: true,
            },
            anotherPlayerId: {
              isMyPlayerData: false,
            },
          },
        },
      ]));

    dynamodbMocks.getItem.mockImplementation((params, callback) =>
      callback(null, {
        Item: {
          ResourceId: { S: 'myUserId' },
          ActiveGames: { SS: ['tournamentId', 'tournament2'] },
        },
      }));
    playerController.getActiveTournaments().then((tournamentList) => {
      expect(dynamodbMocks.getItem).toHaveBeenCalledWith(
        {
          TableName: TABLE_NAME,
          Key: { ResourceId: { S: 'myUserId' } },
        },
        expect.any(Function),
      );
      expect(tournamentList).toEqual(mockSummarizedTournamentList);
      done();
    });
  });

  test('returns an empty array if the player has no active games', (done) => {
    dynamodbMocks.getItem.mockImplementation((params, callback) =>
      callback(null, {
        Item: {
          ResourceId: { S: 'myUserId' },
          ActiveGames: { SS: [] },
        },
      }));
    playerController.getActiveTournaments().then((tournamentList) => {
      expect(dynamodbMocks.getItem).toHaveBeenCalledWith(
        {
          TableName: TABLE_NAME,
          Key: { ResourceId: { S: 'myUserId' } },
        },
        expect.any(Function),
      );
      expect(tournamentList).toEqual([]);
      expect(tournamentController.getTournaments).not.toHaveBeenCalled();
      done();
    });
  });

  test('can return a single tournament santized for the player', () => {
    tournamentController.getTournaments.mockImplementation(() =>
      Promise.resolve([
        {
          Administrator: 'admin',
          CurrentRound: 0,
          ExpiresAt: 1538470166857,
          Players: ['admin', 'myUserId'],
          ResourceId: 'tournamentId',
          RoundCount: 1,
          TickLength: 60,
          NextTick: 1538470066857,
          TournamentName: 'My Game',
          InternalGameState: {
            isPrivate: true,
          },
          GameState: {
            isPublic: true,
          },
          PlayerState: {
            myUserId: {
              isMyPlayerData: true,
              submitted: true,
            },
            admin: {
              isMyPlayerData: false,
            },
          },
        },
      ]));

    playerController.getTournament('tournamentId').then((tournament) => {
      expect(tournament).toEqual({
        Administrator: 'admin',
        CurrentRound: 0,
        ExpiresAt: 1538470166857,
        Players: ['admin', 'myUserId'],
        TournamentId: 'tournamentId',
        RoundCount: 1,
        TickLength: 60,
        NextTick: 1538470066857,
        TournamentName: 'My Game',
        GameState: {
          isPublic: true,
        },
        PlayerState: {
          isMyPlayerData: true,
          submitted: true,
        },
      });
    });
  });
});
