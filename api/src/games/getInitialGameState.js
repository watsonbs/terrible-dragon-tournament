const commonsPot = require('./commonsPot/commonsPotController');

module.exports = (gameType, players, tournamentConfig) => {
  const initalisers = {
    commonsPot: commonsPot.createInitialState,
  };

  return initalisers[gameType](players, tournamentConfig);
};
