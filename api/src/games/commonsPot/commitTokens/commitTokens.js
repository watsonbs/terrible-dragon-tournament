const dynamodb = require('../../../utilities/dynamodb');
const config = require('../../../config');

const commitTokens = (
  playerName,
  tournamentId,
  {
    team, expectedAvailable, expectedNextTick, transferAmount,
  },
) =>
  dynamodb.updateItem({
    TableName: config.TournamentTableName,
    ReturnValues: 'ALL_NEW',
    Key: {
      ResourceId: { S: tournamentId },
    },
    UpdateExpression:
      'ADD #at :transferOut, #ct :transferIn, #pot :transferIn, SET #submitted = :true',
    ExpressionAttributeNames: {
      '#at': `PlayerState.${playerName}.availableTokens`,
      '#ct': `PlayerState.${playerName}.committedTokens`,
      '#pot': `InternalGameState.pot.${team}`,
      '#team': `PlayerState.${playerName}.team`,
      '#submitted': `PlayerState.${playerName}.submitted`,
      '#nextTick': 'NextTick',
    },
    ExpressionAttributeValues: {
      ':transferOut': -transferAmount,
      ':transferIn': transferAmount,
      ':expectedAvailable': expectedAvailable,
      ':expectedNextTick': { N: expectedNextTick },
      ':team': { S: team },
      ':true': { BOOL: true },
      ':commonsPot': { S: 'commonsPot' },
    },
    ConditionExpression:
      '#at = :expectedAvailable AND #team = :team AND #typeId = :commonsPot AND #nextTick = :expectedNextTick',
  });

// TODO - also check that nextTick is in the future
module.exports = commitTokens;
