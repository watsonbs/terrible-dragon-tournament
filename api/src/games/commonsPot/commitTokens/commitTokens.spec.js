const commitTokens = require('./commitTokens');
const dynamodb = require('../../../utilities/dynamodb');

jest.mock('../../../utilities/dynamodb', () => ({
  updateItem: jest.fn(),
}));

describe('commitTokens', () => {
  test('will take tokens out of your available tokens and add them to the pot', () => {
    const mockDynamoResponse = {
      mySuccessResponse: true,
    };
    dynamodb.updateItem.mockReturnValueOnce(Promise.resolve(mockDynamoResponse));
    return commitTokens('aUser', '1234', {
      team: 'red',
      expectedAvailable: 10,
      expectedNextTick: 1000,
      transferAmount: 2,
    }).then((newGameState) => {
      expect(dynamodb.updateItem).toHaveBeenCalledWith({
        ConditionExpression:
          '#at = :expectedAvailable AND #team = :team AND #typeId = :commonsPot AND #nextTick = :expectedNextTick',
        ExpressionAttributeNames: {
          '#at': 'PlayerState.aUser.availableTokens',
          '#ct': 'PlayerState.aUser.committedTokens',
          '#nextTick': 'NextTick',
          '#pot': 'InternalGameState.pot.red',
          '#submitted': 'PlayerState.aUser.submitted',
          '#team': 'PlayerState.aUser.team',
        },
        ExpressionAttributeValues: {
          ':commonsPot': { S: 'commonsPot' },
          ':expectedAvailable': 10,
          ':expectedNextTick': { N: 1000 },
          ':team': { S: 'red' },
          ':transferIn': 2,
          ':transferOut': -2,
          ':true': { BOOL: true },
        },
        Key: { ResourceId: { S: '1234' } },
        ReturnValues: 'ALL_NEW',
        TableName: 'terrible-dragon_tournaments',
        UpdateExpression:
          'ADD #at :transferOut, #ct :transferIn, #pot :transferIn, SET #submitted = :true',
      });
      expect(newGameState).toEqual(mockDynamoResponse);
    });
  });
});
