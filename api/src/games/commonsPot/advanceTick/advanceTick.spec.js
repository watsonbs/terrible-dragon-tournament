const dynamodb = require('../../../utilities/dynamodb');
const advanceTick = require('./advanceTick');
const config = require('../../../config');

jest.mock('../../../utilities/dynamodb', () => ({
  getItem: jest.fn(),
  updateItem: jest.fn(),
}));

describe('advanceTick', () => {
  test('resets for the next round without a Black team', () => {
    dynamodb.getItem.mockReturnValueOnce(Promise.resolve({
      Item: {
        ResourceId: { S: '1234' },
        TickLength: { N: 600 },
        NextTick: { N: 600 },
        InternalGameState: {
          M: {
            pot: {
              M: {
                red: { N: 2 },
                blue: { N: 4 },
              },
            },
          },
        },
        GameState: {
          M: {
            typeId: { S: 'commonsPot' },
            previousRounds: { SS: [] },
            redPlayers: {
              M: {
                submitted: { SS: ['playerA', 'playerB'] },
                unsubmitted: { SS: [] },
              },
            },
            bluePlayers: {
              M: {
                submitted: { SS: ['playerC', 'playerD'] },
                unsubmitted: { SS: [] },
              },
            },
          },
        },
        PlayerState: {
          M: {
            playerA: {
              M: {
                availableTokens: { N: 9 },
                committedTokens: { N: 1 },
                submitted: { BOOL: true },
                team: { S: 'red' },
              },
            },
            playerB: {
              M: {
                availableTokens: { N: 9 },
                committedTokens: { N: 1 },
                submitted: { BOOL: true },
                team: { S: 'red' },
              },
            },
            playerC: {
              M: {
                availableTokens: { N: 9 },
                committedTokens: { N: 1 },
                submitted: { BOOL: true },
                team: { S: 'blue' },
              },
            },
            playerD: {
              M: {
                availableTokens: { N: 7 },
                committedTokens: { N: 3 },
                submitted: { BOOL: true },
                team: { S: 'blue' },
              },
            },
          },
        },
      },
    }));

    dynamodb.updateItem.mockReturnValueOnce(Promise.resolve({
      updateResponse: true,
    }));

    return advanceTick('1234').then(() => {
      expect(dynamodb.getItem).toHaveBeenCalledWith({
        TableName: config.TournamentTableName,
        Key: {
          ResourceId: { S: '1234' },
        },
        ProjectionExpression: 'TickLength,InternalGameState,GameState,PlayerState,NextTick',
      });

      expect(dynamodb.updateItem).toHaveBeenCalledWith({
        ExpressionAttributeNames: {
          '#bluePot': 'InternalGameState.pot.blue',
          '#blueSubmitted': 'GameState.bluePlayers.submitted',
          '#blueUnsubmitted': 'GameState.bluePlayers.unsubmitted',
          '#nextTick': 'NextTick',
          '#playerACommitted': 'PlayerState.playerA.committedTokens',
          '#playerASubmitted': 'PlayerState.playerA.submitted',
          '#playerBCommitted': 'PlayerState.playerB.committedTokens',
          '#playerBSubmitted': 'PlayerState.playerB.submitted',
          '#playerCCommitted': 'PlayerState.playerC.committedTokens',
          '#playerCSubmitted': 'PlayerState.playerC.submitted',
          '#playerDCommitted': 'PlayerState.playerD.committedTokens',
          '#playerDSubmitted': 'PlayerState.playerD.submitted',
          '#redPot': 'InternalGameState.pot.red',
          '#redSubmitted': 'GameState.redPlayers.submitted',
          '#redUnsubmitted': 'GameState.redPlayers.unsubmitted',
        },
        ExpressionAttributeValues: {
          ':bluePlayers': {
            SS: ['playerC', 'playerD'],
          },
          ':empty': {
            SS: [],
          },
          ':false': {
            BOOL: false,
          },
          ':nextTick': {
            N: 1200,
          },
          ':redPlayers': {
            SS: ['playerA', 'playerB'],
          },
          ':zero': {
            N: 0,
          },
        },
        Key: {
          ResourceId: {
            S: '1234',
          },
        },
        TableName: 'terrible-dragon_tournaments',
        UpdateExpression:
          'SET #nextTick = :nextTick, #playerACommitted = :zero, #playerASubmitted = :false, #playerBCommitted = :zero, #playerBSubmitted = :false, #playerCCommitted = :zero, #playerCSubmitted = :false, #playerDCommitted = :zero, #playerDSubmitted = :false, #redPot = :zero, #redSubmitted = :empty, #redUnsubmitted = :redPlayers, #bluePot = :zero, #blueSubmitted = :empty, #blueUnsubmitted = :bluePlayers',
      });
    });
  });
});
