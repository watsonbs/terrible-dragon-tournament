const dynamodb = require('../../../utilities/dynamodb');
const dynamoConverter = require('../../../utilities/dynamoConverter');
const config = require('../../../config');

const generateUpdateParams = (tournamentId, tournamentData) => {
  const playerNames = Object.keys(tournamentData.PlayerState);
  let UpdateExpression = 'SET #nextTick = :nextTick';

  const teams = playerNames.reduce((reduction, playerName) => {
    const teamName = tournamentData.PlayerState[playerName].team;
    const teamObject = { ...reduction };
    teamObject[teamName] = teamObject[teamName] || [];
    teamObject[teamName].push(playerName);
    return teamObject;
  }, {});

  const playerExpressionAttributeNames = playerNames.reduce(
    (exressionAttributeNames, playerName) => {
      UpdateExpression = `${UpdateExpression}, #${playerName}Committed = :zero, #${playerName}Submitted = :false`;
      return {
        ...exressionAttributeNames,
        [`#${playerName}Committed`]: `PlayerState.${playerName}.committedTokens`,
        [`#${playerName}Submitted`]: `PlayerState.${playerName}.submitted`,
      };
    },
    {},
  );

  const teamNames = Object.keys(teams);

  const gameExpressionAttributeNames = teamNames.reduce(
    (attributeNames, teamName) => {
      UpdateExpression = `${UpdateExpression}, #${teamName}Pot = :zero, #${teamName}Submitted = :empty, #${teamName}Unsubmitted = :${teamName}Players`;
      return {
        ...attributeNames,
        [`#${teamName}Pot`]: `InternalGameState.pot.${teamName}`,
        [`#${teamName}Submitted`]: `GameState.${teamName}Players.submitted`,
        [`#${teamName}Unsubmitted`]: `GameState.${teamName}Players.unsubmitted`,
      };
    },
    { '#nextTick': 'NextTick' },
  );

  const ExpressionAttributeValues = {
    ':zero': { N: 0 },
    ':empty': { SS: [] },
    ':redPlayers': { SS: teams.red },
    ':bluePlayers': { SS: teams.blue },
    ':false': { BOOL: false },
    ':nextTick': { N: tournamentData.NextTick + tournamentData.TickLength },
  };

  const updateParams = {
    TableName: config.TournamentTableName,
    Key: {
      ResourceId: { S: tournamentId },
    },
    ExpressionAttributeValues,
    ExpressionAttributeNames: {
      ...gameExpressionAttributeNames,
      ...playerExpressionAttributeNames,
    },
    UpdateExpression,
  };

  return updateParams;
};

const advanceTick = tournamentId =>
  dynamodb
    .getItem({
      TableName: config.TournamentTableName,
      Key: {
        ResourceId: { S: tournamentId },
      },
      ProjectionExpression: 'TickLength,InternalGameState,GameState,PlayerState,NextTick',
    })
    .then(dynamoResponse => dynamoConverter.convertFromDynamo(dynamoResponse.Item))
    .then(tournamentData => generateUpdateParams(tournamentId, tournamentData))
    .then(dynamodb.updateItem);

module.exports = advanceTick;
