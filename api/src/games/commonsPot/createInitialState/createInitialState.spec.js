const createInitialState = require('./createInitialState');
const shuffle = require('../../../utilities/shuffle');

jest.mock('../../../utilities/shuffle', () => jest.fn(array => array));

describe('initialise commonsPot', () => {
  const dateNow = Date.now;
  beforeEach(() => {
    Date.now = jest.fn(() => 100);
  });
  afterEach(() => {
    Date.now = dateNow;
  });
  test('initialises an even number of players', () => {
    const players = ['player1', 'player2', 'player3', 'player4'];
    const tournamentConfig = { tickLength: 600 };
    const initalState = createInitialState(players, tournamentConfig);

    expect(shuffle).toHaveBeenCalledWith(players);

    expect(initalState).toEqual({
      GameState: {
        typeId: 'commonsPot',
        bluePlayers: { submitted: [], unsubmitted: ['player1', 'player3'] },
        previousRounds: [],
        redPlayers: { submitted: [], unsubmitted: ['player2', 'player4'] },
      },
      InternalGameState: { pot: { blue: 0, red: 0 } },
      NextTick: 700,
      PlayerState: {
        player1: {
          availableTokens: 10,
          committedTokens: 0,
          team: 'blue',
          submitted: false,
        },
        player2: {
          availableTokens: 10,
          committedTokens: 0,
          team: 'red',
          submitted: false,
        },
        player3: {
          availableTokens: 10,
          committedTokens: 0,
          team: 'blue',
          submitted: false,
        },
        player4: {
          availableTokens: 10,
          committedTokens: 0,
          team: 'red',
          submitted: false,
        },
      },
    });
  });

  test('initialises an odd number of players', () => {
    const players = ['player1', 'player2', 'player3', 'player4', 'player5'];
    const tournamentConfig = { tickLength: 600 };
    const initalState = createInitialState(players, tournamentConfig);

    expect(shuffle).toHaveBeenCalledWith(players);

    expect(initalState).toEqual({
      GameState: {
        typeId: 'commonsPot',
        blackPlayers: { submitted: [], unsubmitted: ['player1'] },
        bluePlayers: { submitted: [], unsubmitted: ['player3', 'player5'] },
        previousRounds: [],
        redPlayers: { submitted: [], unsubmitted: ['player2', 'player4'] },
      },
      InternalGameState: { pot: { black: 0, blue: 0, red: 0 } },
      NextTick: 700,
      PlayerState: {
        player1: {
          availableTokens: 10,
          committedTokens: 0,
          team: 'black',
          submitted: false,
        },
        player2: {
          availableTokens: 10,
          committedTokens: 0,
          team: 'red',
          submitted: false,
        },
        player3: {
          availableTokens: 10,
          committedTokens: 0,
          team: 'blue',
          submitted: false,
        },
        player4: {
          availableTokens: 10,
          committedTokens: 0,
          team: 'red',
          submitted: false,
        },
        player5: {
          availableTokens: 10,
          committedTokens: 0,
          team: 'blue',
          submitted: false,
        },
      },
    });
  });
});
