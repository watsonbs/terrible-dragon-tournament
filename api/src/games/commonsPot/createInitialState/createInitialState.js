const shuffle = require('../../../utilities/shuffle');

module.exports = (players, tournamentConfig) => {
  const shuffledPlayers = shuffle(players);
  const isOddPlayerCount = Boolean(shuffledPlayers.length % 2);
  const gameState = {
    InternalGameState: {
      pot: {
        red: 0,
        blue: 0,
      },
    },
    GameState: {
      typeId: 'commonsPot',
      previousRounds: [],
      redPlayers: {
        submitted: [],
        unsubmitted: [],
      },
      bluePlayers: {
        submitted: [],
        unsubmitted: [],
      },
    },
    NextTick: Date.now() + tournamentConfig.tickLength,
  };

  if (isOddPlayerCount) {
    gameState.GameState.blackPlayers = {
      submitted: [],
      unsubmitted: [],
    };
    gameState.InternalGameState.pot.black = 0;
  }

  gameState.PlayerState = shuffledPlayers.reduce((state, playerName, index) => {
    const defaultTeam = index % 2 ? 'red' : 'blue';
    const team = index === 0 && isOddPlayerCount ? 'black' : defaultTeam;

    gameState.GameState[`${team}Players`].unsubmitted.push(playerName);

    return {
      ...state,
      [playerName]: {
        availableTokens: 10,
        committedTokens: 0,
        submitted: false,
        team,
      },
    };
  }, {});

  return gameState;
};
