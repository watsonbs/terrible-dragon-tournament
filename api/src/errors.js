const Invalid = new Error('Invalid Request');
Invalid.statusCode = 400;

const Unauthorized = new Error('Unauthorized, please log in');
Unauthorized.statusCode = 401;

const Forbidden = new Error('Forbidden. You do not have permission to perform this action');
Forbidden.statusCode = 403;

const NotFound = new Error('Not Found');
NotFound.statusCode = 404;

module.exports = {
  Invalid,
  Unauthorized,
  Forbidden,
  NotFound,
};
