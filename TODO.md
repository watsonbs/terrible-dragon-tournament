# Tier 1

* Some actual games

# Tier 2

* Account pages
* Root page for unsigned in
* About and Rules
* Footer
* Archive Games
* getArchivedGames
* API error handling (esp. rollback de-normalised changes)
* change join/create controls to not be FABs
* Extend expiration time on action
* CloudFront for API Gateway
* Get environment in app
* App Error Handling

# Tier 3

* Bring Dynamo into CloudFormation
* Bring Authorization into CloudFormation
* Restrict Permissions for Lambdas
* set player and/or tournament limit
* Password protection for games
* Easier join method
* CORS settings into CloudFormation
* Better CORS settings
* Terrible dragon email
